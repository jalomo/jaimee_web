<?php
//session_start();
// Required if your environment does not handle autoloading

require_once(APPPATH.'vendor/autoload.php');
//require __DIR__ . '/vendor/autoload.php';
//require site_url('/vendor/autoload.php') ;
// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

require APPPATH.'../vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;

class Usuarios extends CI_Controller {
    //const USER_ERROR_DIR = APPPATH.'logs/Site_User_errors.log';
	function __construct()
	{
		parent::__construct();
         
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->helper('array');
        $this->load->helper('url');
        $this->load->helper('file');
        $this->load->helper('correo');

        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library("pagination");
        $this->load->library('upload');
        $this->load->library('email');

        $this->load->model('usuarios_model');
        $this->load->model('servicios_model');
        $this->load->model('operadores_model');
        $this->load->model('principal');
        $this->output->set_template('template_custom');

        date_default_timezone_set('America/Mexico_City');
	}

    public function index()
    {    
        $_SESSION['hdOpcion'] = 1;
        if(isset($this->session->userdata['loggedin'])){
            $result = $this->servicios_model->get_servicio_usuario($this->session->userdata['loggedin']['id']);

            if($result){
                $data['datos_servicio'] = $this->servicios_model->get_infoservicio_usuario($this->session->userdata['loggedin']['id']);
                $data['idServicio'] =  $data['datos_servicio'][0]->servicioId;
            }else{
                $data['datos_servicio'] = array();
                $data['idServicio'] =0;
            }
            $tipo_auto = $this->db->get('tipo_auto')->result();
            $data['drop_tipo_auto'] = form_dropdown('tipo_auto',array_combos($tipo_auto,'id','tipo',TRUE),"",'class="form-control" id="tipo_auto" ');
            $this->blade->render('servicios/elegir_auto',$data);
        }
        else 
             $this->load->view('usuarios/login');
    }
    public function cancelar_servicio()
    {
        $idServicio = $this->input->post('idServicio');
        $result = $this->servicios_model->update_servicio_usuario($idServicio, 3);
       echo 1;exit();
    }

	public function indexBk()
	{    
        $_SESSION['hdOpcion'] = 1;
        if(isset($this->session->userdata['loggedin'])){
            $result = $this->servicios_model->get_servicio_usuario($this->session->userdata['loggedin']['id']);
           
            if ($result) 
            {
                //echo $result;
                $_SESSION['solicitado'] = 1;
                $result = $this->servicios_model->get_infoservicio_usuario($this->session->userdata['loggedin']['id']);

                $data['latitud'] = $result[0]->servicioLatuitudInicio;
                $data['longitud'] = $result[0]->servicioLongitudInicio;
                $data['encontrado'] = 1;
                $data['idServicio'] =  $result[0]->servicioId;
                $this->load->view('servicios/index', $data);
            }else
            {
                $_SESSION['solicitado']  = 0;
                $this->load->view('servicios/index');
            }
        }
        else 
             $this->load->view('usuarios/login');
	}
    public function proceso_login_usuario($data = null) {
        $this->form_validation->set_rules('usuario', 'Usuario', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_message('required', 'Oops el campo %s es requerido!!');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('usuarios/login');
        } else {
            $datalog = array(
                'usuario' => $this->input->post('usuario'),
                'password' => $this->input->post('password')
            );

            $result = $this->usuarios_model->login($datalog);

            if ($result == TRUE) {
                $username = $this->input->post('usuario');
                $result = $this->usuarios_model->leer_informacion_usuario($username);
                if ($result != false) {
                    $session_data = array(
                        'usuario' => $result[0]->usuarioNombre,
                        'telefono' => $result[0]->usuarioCelular,
                        'id' => $result[0]->usuarioId,
                        'estado' => $result[0]->estado,
                        'municipio' => $result[0]->municipio, 
                        'tipoUsuario' => $result[0]->tipoUsuario 
                    );
                    $this->session->set_userdata('loggedin', $session_data);
                    

                    if($result[0]->tipoUsuario == 2)
                    {
                        if($_SESSION['hdOpcion']==4)
                            $this->estadistico();
                        else
                            if($_SESSION['hdOpcion']==5)
                                $this->estadistico_bases();
                            else
                                redirect("usuarios/index_base");
                    }
                    else
                    {
                        if($_SESSION['hdOpcion'] == 0)
                            $this->finalizar();
                        else 
                        {
                            if($_SESSION['hdOpcion'] == 1)
                                $this->index();
                            else
                            {
                                if($_SESSION['hdOpcion']== 2)
                                    $this->misservicios();
                                else
                                {
                                    if($_SESSION['hdOpcion']== 3)
                                        $this->comentarios();
                                    else 
                                        $this->index();
                                }
                            }
                        }
                    }
                }
            } else {
                $data = array(
                    'error_message' => 'Usuario o contraseña inválida'
                );
            
                $this->load->view('usuarios/login', $data);
            }
        }
    }

    public function nuevo_usuario_registro() {
        $this->form_validation->set_rules('usuario', 'Usuario', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('confirmacion', 'Confirmacion', 'trim|required|matches[password]');
        $this->form_validation->set_rules('telefono', 'Telefono', 'trim|required');
        $this->form_validation->set_rules('codigo', 'Codigo', 'trim|required');
        //$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_message('required', 'Oops el campo %s es requerido!!');
        $this->form_validation->set_message('matches', 'Oops el campo %s no coincide con el de %s!!');
        //$this->form_validation->set_message('valid_email', 'Oops el correo electrónico es inválido!!');

        $data['usuario'] = $this->input->post('usuario');
        $data['password'] = $this->input->post('password');
        $data['confirmacion'] = $this->input->post('confirmacion');
        $data['codigo'] = $this->input->post('codigo');
        $data['telefono'] = $this->input->post('telefono');
        //print $_SESSION['domicilio']; die();
        $data['email'] = $this->input->post('email');
        //$data['tipoUsuario'] = 2;

        $pos = strpos($this->input->post('usuario'), ' ');

        if ($this->form_validation->run() == FALSE) {
            
            $this->load->view('usuarios/registro', $data);
        } else {
            if($pos)
            {
                $data['message_display'] = 'El usuario no debe contener espacios es blanco';
                $this->load->view('usuarios/registro', $data);
            }else{
                if($this->input->post('codigo') != $_SESSION['codigo']){
                    $data['message_codigo'] = 'El codigo es incorrecto, favor de verificarlo con el SMS que se envió al telefóno';
                    $this->load->view('usuarios/registro', $data);
                }else{
                    $data = array(
                        'usuarioNombre'   =>  $this->input->post('usuario'),
                        'usuarioCelular'   =>  $this->input->post('telefono'),
                        'usuarioStatus' => 1, 
                        'usuarioPassword' => $this->input->post('password'),
                        'email'   =>  $this->input->post('email'),
                        'tipoUsuario'   =>  2,
                        'descripcionBase'   =>  "usuario",
                        'estado'   =>  "COL",
                        'municipio'   =>  "007",
                    );
                    //$result = $this->usuarios_model->insert_usuario($data);
                    $result = $this->usuarios_model->update_insert_usuario($data);
                    if ($result == TRUE) {
                        $data['message_display'] = 'Usuario registrado exitosamente!';

                        if( isset($_SESSION['codigo']))
                            unset($_SESSION['codigo']);
                        //$this->load->view('usuarios/login', $data);
                        $this->proceso_login_usuario($data);
                    } else {
                        $data['message_display'] = 'El usuario ya existe!';
                        $this->load->view('usuarios/registro', $data);
                    }
                }
            }
        }
    }

    public function contrasena() {
        $this->output->unset_template();

        $username = $this->input->post('texto');
        $tipo = 1;

        $result = $this->usuarios_model->contrasena($username,$tipo);
        if ($result != false) {
            $usuario = trim($result[0]->usuarioNombre);
            $password = trim($result[0]->usuarioPassword);
            $tel = trim($result[0]->usuarioCelular);

            $accountSid = 'ACe8ab4310e97e582af509109e0d358ae6';
            $authToken = '04278a55999b7f0967f24b1b822ff3b8';
            $twilioNumber = '+17027103658';
            
            $telefono = "+52".$tel;
            //$codigo = $this->randomNumber(4);
            $message = 'Tus datos se acceso a centraldebases.com son Usuario:'.$usuario.' Contraseña:'.$password;
            //$message = "Tu codigo de acceso a centraldebases.com es el siguiente: ".$codigo;

            $client = new Client($accountSid, $authToken);

            try {
                $client->messages->create(
                    $telefono,
                    [
                        "body" => $message,
                        "from" => $twilioNumber
                    ]
                );
                
                $data_codigo['return'] = 1;
                $data_codigo['codigo'] = $tel;
                echo json_encode($data_codigo);

            } catch (TwilioException $e) {
                $data_codigo['return'] = 0;
                $data_codigo['codigo'] = $e->getMessage();

                echo json_encode($data_codigo);

            }catch(Exception $ex){
                $data_codigo['return'] = 0;
                $data_codigo['codigo'] = $ex->getMessage();

                echo json_encode($data_codigo);
            }
        }else
        {
            $data_codigo['return'] = 2;
            $data_codigo['codigo'] = "No existe registro con los datos proporcionados";
            echo json_encode($data_codigo);
        }
    }

    public function mostrar_registro() {
        $this->load->view('usuarios/registro');
    }

    public function mostrar_registro_operador() {
        $this->load->view('usuarios/operador');
    }


    public function file_check($str,$f){
        $allowed_mime_type_arr = array('image/jpg','image/jpeg','image/png');
        $mime = get_mime_by_extension($_FILES[$f]['name']);

        if(isset($_FILES[$f]['name']) && $_FILES[$f]['name']!=""){
            if(in_array($mime, $allowed_mime_type_arr)){
                return true;
            }else{
                $this->form_validation->set_message('file_check', 'Oops debes seleccionar solamente imagenes jpg/png!!');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'Oops debes seleccionar una imagen!!');
            return false;
        }
    }

    public function nuevo_operador_registro() {
       // print_r($_FILES);
        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('telefono', 'Telefono', 'trim|required');
        $this->form_validation->set_rules('usuario', 'Usuario', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[confirmacion]');
        $this->form_validation->set_rules('confirmacion', 'Confirmacion', 'trim|required');

        $existeplaca = $this->input->post('hdExiste');

        if($existeplaca == "0")
        {
            $this->form_validation->set_rules('sitio', 'Sitio', 'trim|required');
            $this->form_validation->set_rules('numero', 'Número', 'trim|required');
            $this->form_validation->set_rules('placas', 'Placas', 'trim|required');
            $this->form_validation->set_rules('modelo', 'Modelo', 'trim|required');
            $this->form_validation->set_rules('cmbTipo', 'Tipo', 'trim|required');
            $this->form_validation->set_rules('color', 'Color', 'trim|required');
            $this->form_validation->set_rules('marca', 'Marca', 'trim|required');
            $this->form_validation->set_rules('linea', 'Linea', 'trim|required');
        }

        $a = "archivo";
        $this->form_validation->set_rules('archivo', '', 'callback_file_check['.$a.']');

        
        $this->form_validation->set_message('required', 'Oops el campo %s es requerido!!');
        
        $this->form_validation->set_message('matches', 'Oops el campo %s no coincide con el de %s!!');
        
        $data['nombre'] = $this->input->post('nombre');
        $data['usuario'] = $this->input->post('usuario');
        $data['password'] = $this->input->post('password');
        $data['confirmacion'] = $this->input->post('confirmacion');
        $data['sitio'] = $this->input->post('sitio');
        $data['numero'] = $this->input->post('numero');
        $data['telefono'] = $this->input->post('telefono');
        $data['placas'] = $this->input->post('placas');
        $data['modelo'] = $this->input->post('modelo');
        $data['cmbTipo'] = $this->input->post('cmbTipo');
        $data['color'] = $this->input->post('color');
        $data['marca'] = $this->input->post('marca');
        $data['linea'] = $this->input->post('linea');
        $data['hdExiste'] = $this->input->post('hdExiste');
        

        $pos = strpos($this->input->post('usuario'), ' ');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('usuarios/operador', $data);
        } else {
            if($pos)
            {
                $data['message_display'] = 'El usuario no debe contener espacios es blanco';
                $this->load->view('usuarios/operador', $data);
            }else{

                $data = array(
                    'usuario' => $this->input->post('usuario') 
                );

                $result = $this->operadores_model->existe_operador($data);
                if ($result == FALSE) {
                    $name = date('dmyHms').'_'.str_replace(" ", "", $_FILES['archivo']['name']);
                    //$name = date('dmy').'_'.$this->input->post('usuario').'.'.$_FILES['archivo']['type'];
                    $path_to_save = 'statics/fotos/';

                    if(!file_exists($path_to_save)){
                      mkdir($path_to_save, 0777, true);
                    }
                    move_uploaded_file($_FILES['archivo']['tmp_name'], 'central_admin/'.$path_to_save.$name);

                    $data = array(
                        'operadorNombreCompleto'   =>  $this->input->post('nombre'),
                        'operadorUsuario'   =>  $this->input->post('usuario'),
                        'operdorPassword' => $this->input->post('password'), 
                        'OperadorImagen' =>  "",
                        'OperadorImagen2' =>  $path_to_save.$name,
                        'operadorTelefono' => $this->input->post('telefono'),
                        'imei' =>  "1", 
                        'operadorStatus' => "1" 
                    );

                    $result = $this->operadores_model->insert_operador($data);

                    if ($result == TRUE) {
                        $idOperador = $result;

                        $result = $this->operadores_model->existe_auto( $this->input->post('placas'));
                        if ($result == FALSE) {
                            $descrcipcion = $this->input->post('marca').' '. $this->input->post('linea').' '. $this->input->post('modelo');
                            $data = array(
                                'autosPlacas'   =>  $this->input->post('placas'),
                                'autosDescripcion'   =>  $descrcipcion,
                                'autosImagen' =>  '',
                                'autosNick' =>  $this->input->post('numero'),
                                'autosColor' => $this->input->post('color'), 
                                'autosSitio' => $this->input->post('sitio'),
                                'autosTipo' =>  $this->input->post('cmbTipo'), 
                                'autosStatus' => "1" 
                            );

                            $result = $this->operadores_model->insert_auto($data);
                            if ($result == FALSE) {
                                $data['message_display'] = 'Ocurrió un error, por favor intente más tarde.';
                                $this->load->view('usuarios/operador', $data);
                            }else
                                $idAuto = $result;
                        }else
                            $idAuto = $result[0]->autosId;

                        $data = array(
                            'OPIdOperador'   =>  $idOperador,
                            'OPIdAuto'   =>  $idAuto,
                            'OPStatust' =>  "1",
                            'OPLatitud' =>  "19.25566608394677", 
                            'OPLongitud' => "-103.7515141069153", 
                            'token' => "",
                            'imei' =>  "1"
                        );

                        $result = $this->operadores_model->insert_operador_auto($data);
                        if ($result == FALSE) {
                            $data['message_display'] = 'Ocurrió un error, por favor intente más tarde.';
                            $this->load->view('usuarios/operadores', $data);
                        }
                        else{

                            $config = Array(
                                'protocol' => 'smtp',
                                'smtp_host' => 'ssl://smtp.googlemail.com',
                                'smtp_port' => 465,
                                'smtp_user' => 'contactocentraldebases@gmail.com',
                                'smtp_pass' => 'e}1^nT3f,5tY6~',
                                'mailtype'  => 'html', 
                                'charset'   => 'utf-8'
                            );

                            /*$config = Array(
                                'protocol' => 'smtp',
                                'smtp_host' => 'servidor6501.sd.controladordns.com',
                                'smtp_port' => 465,
                                'smtp_user' => 'contacto@centraldebases.com',
                                'smtp_pass' => 'e}1^nT3f,5tY6~',
                                'mailtype'  => 'html', 
                                'charset'   => 'utf-8'
                            );*/

                            $this->load->library('email', $config);
                            $this->email->set_newline("\r\n");

                            $this->email->from('contacto@centraldebases.com', 'Central de Bases');
                            $this->email->to('vargaslagos@gmail.com');
                            //$this->email->to('pedrazarey@gmail.com');

                            $this->email->subject('Nuevo operador registrado');

                            if($this->input->post('cmbTipo') == 1)
                                $tipo = "AUTOMÓVIL";
                            else
                                $tipo = "CAMIONETA";


                            $mensaje =  "Nombre: " . $this->input->post('nombre') ."\r\n " .
                                        "Teléfono: " . $this->input->post('telefono') ." \r\n " .
                                        "Usuario: " . $this->input->post('usuario') ." \r\n " .
                                        "Sitio: " . $this->input->post('sitio')." ".$this->input->post('numero') ." \r\n " .
                                        "Placas: " . $this->input->post('placas') ."\r\n ". 
                                        "Modelo: " . $this->input->post('modelo') ." \r\n ". 
                                        "Tipo: " . $tipo . " ". 
                                        "Color: " . $this->input->post('color') ." \r\n ". 
                                        "Marca: " . $this->input->post('marca') ." \r\n ". 
                                        "Línea: " . $this->input->post('linea') ;

                            $this->email->message(nl2br($mensaje));

                           // $this->email->message('Hola, esta es una prueba');

                            //$this->email->send();
                           
                            /*if (!$this->email->send())
                            {
                              $this->email->print_debugger(array('headers'));
                            }
                            else 
                            {*/
                                $data['message_display'] = 'Operador registrado exitosamente!';

                                $this->load->view('usuarios/operador', $data);
                            //}
                        }
                        
                    } else {
                        $data['message_display'] = 'Ocurrió un error, por favor intente más tarde.';
                        $this->load->view('usuarios/operador', $data);
                    }
                }
                else
                {
                    $data['message_display'] = 'El usuario ya existe!';
                    $this->load->view('usuarios/operador', $data);
                }
            }
        }
    }

    public function nuevovehiculo() {
        $this->load->view('usuarios/nuevovehiculo');
    }

    public function nuevo_auto_registro() {
       // print_r($_FILES);
        $this->form_validation->set_rules('sitio', 'Sitio', 'trim|required');
        $this->form_validation->set_rules('numero', 'Número', 'trim|required');
        $this->form_validation->set_rules('placas', 'Placas', 'trim|required');
        $this->form_validation->set_rules('modelo', 'Modelo', 'trim|required');
        $this->form_validation->set_rules('cmbTipo', 'Tipo', 'trim|required');
        $this->form_validation->set_rules('color', 'Color', 'trim|required');
        $this->form_validation->set_rules('marca', 'Marca', 'trim|required');
        $this->form_validation->set_rules('linea', 'Linea', 'trim|required');
        
        $this->form_validation->set_message('required', 'Oops el campo %s es requerido!!');
        
        $data['sitio'] = $this->input->post('sitio');
        $data['numero'] = $this->input->post('numero');
        $data['telefono'] = $this->input->post('telefono');
        $data['placas'] = $this->input->post('placas');
        $data['modelo'] = $this->input->post('modelo');
        $data['cmbTipo'] = $this->input->post('cmbTipo');
        $data['color'] = $this->input->post('color');
        $data['marca'] = $this->input->post('marca');
        $data['linea'] = $this->input->post('linea');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('usuarios/nuevovehiculo', $data);
        } else {
            $result = $this->operadores_model->existe_auto( $this->input->post('placas'));
            if ($result == FALSE) {
                $descrcipcion = $this->input->post('marca').' '. $this->input->post('linea').' '. $this->input->post('modelo');
                $data = array(
                    'autosPlacas'   =>  $this->input->post('placas'),
                    'autosDescripcion'   =>  $descrcipcion,
                    'autosImagen' =>  '',
                    'autosNick' =>  $this->input->post('numero'),
                    'autosColor' => $this->input->post('color'), 
                    'autosSitio' => $this->input->post('sitio'),
                    'autosTipo' =>  $this->input->post('cmbTipo'), 
                    'autosStatus' => "1" 
                );

                $result = $this->operadores_model->insert_auto($data);
                if ($result == FALSE) {
                    $data['message_display'] = 'Ocurrió un error, por favor intente más tarde.';
                    $this->load->view('usuarios/registrovehiculo', $data);
                }else
                {
                    $data['message_display'] = 'Vehiculo registrado exitosamente!';
                    $this->load->view('usuarios/registrovehiculo', $data);
                }
            }
        }
    }


    public function login()
    {
        $_SESSION['hdOpcion'] = 1;
        $this->load->view('usuarios/login');
    }

    public function logout() {
        if(isset($this->session->userdata['loggedin'])){
            if($this->session->userdata['loggedin']['tipoUsuario'] != 2)
            {
                $sess_array = array(
                    'usuario' => ''
                );
                
                if( isset($_SESSION['telefono']))
                    unset($_SESSION['telefono']);
                if( isset($_SESSION['codigo']))
                    unset($_SESSION['codigo']);
                if( isset($_SESSION['latitud']))
                    unset($_SESSION['latitud']);
                if( isset($_SESSION['longitud']))
                    unset($_SESSION['longitud']);
                if( isset($_SESSION['domicilio']))
                    unset($_SESSION['domicilio']);
                if( isset($_SESSION['hdOpcion']))
                    unset($_SESSION['hdOpcion']);
                if( isset($_SESSION['solicitado']))
                    unset($_SESSION['solicitado']);
                $result = $this->servicios_model->update_servicio_usuario(0,3);
                $this->session->unset_userdata('loggedin', $sess_array);
                $this->index();
            }
            else
            {
                $this->session->unset_userdata('loggedin', $sess_array);
                $this->index_base();
            }
        }

    }

    public function existe()
    {
        $usuarioNombre = $this->input->post('usuario');
        $this->output->unset_template();
        try{
            $pos = strpos($usuarioNombre, ' ');

            if($pos)
            {
                $data_codigo['return'] = 3;
                $data_codigo['codigo'] = "El usuario no debe contener espacios en blanco";
                echo json_encode($data_codigo);
            }else{
                $data = array(
                    'usuario' => $usuarioNombre
                    //'telefono' => $usuarioCelular
                );
                $result = $this->usuarios_model->existe($data);

                if ($result == false) {
                    $data_codigo['return'] = 0;
                    echo json_encode($data_codigo);
                }else{
                    $data_codigo['return'] = 1;
                    echo json_encode($data_codigo);
                }
            }
        
        }catch(Exception $ex){
            $data_codigo['return'] = 2;
            $data_codigo['codigo'] = $ex->getMessage();
            echo json_encode($data_codigo);
        }
    }

    public function existeNumero()
    {
        $usuarioCelular = $this->input->post('numero');
        $this->output->unset_template();
        try{
            
            $data = array(
                //'usuario' => $usuarioNombre
                'telefono' => $usuarioCelular
            );
            $result = $this->usuarios_model->existeNumero($data);

            if ($result == false) {
                $data_codigo['return'] = 0;
                echo json_encode($data_codigo);
            }else{
                $data_codigo['return'] = 1;
                echo json_encode($data_codigo);
            }
        }catch(Exception $ex){
            $data_codigo['return'] = 2;
            $data_codigo['codigo'] = $ex->getMessage();
            echo json_encode($data_codigo);
        }
    }
    
    public function existe_operador($usuarioNombre)
    {
        $this->output->unset_template();
        try{
            $data = array(
                'usuario' => $usuarioNombre
            );
            $result = $this->operadores_model->existe_operador($data);

            if ($result == false) {
                $data_codigo['return'] = 0;
                echo json_encode($data_codigo);
            }else{
                $data_codigo['return'] = 1;
                echo json_encode($data_codigo);
            }
        
        }catch(Exception $ex){
            $data_codigo['return'] = 2;
            $data_codigo['codigo'] = $ex->getMessage();
            echo json_encode($data_codigo);
        }
    }

    public function solicitar_codigo($tel)
    {
        $this->output->unset_template();
        if( isset($_SESSION['codigo']))
        {
            $data_codigo['return'] = 2;
            $data_codigo['codigo'] = $_SESSION['codigo'];
            
            echo json_encode($data_codigo);
        }
        else{

            
                $codigo = $this->randomNumber(4);

                $data = array(
                    'usuarioNombre'   =>  $this->input->post('usuario'),
                    'usuarioCelular'   =>  $this->input->post('telefono'),
                    'usuarioStatus' => 2, 
                    'usuarioPassword' => $this->input->post('password'), 
                    'codigo' => $codigo
                );

                $result = $this->usuarios_model->insert_usuario($data);
                
                /*$data_codigo['return'] = 1;
                $data_codigo['codigo'] = $codigo;
                $_SESSION['codigo'] = $codigo;
                $_SESSION['telefono'] = $tel;

                echo json_encode($data_codigo);*/

                /*try {
                    
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_URL => 'http://uniendogeneraciones.com.mx/app_mensajes/index.php/api/enviar_notificacion?codigo='.$codigo.'&numero='.$tel,
                        CURLOPT_USERAGENT => 'cURL Request'
                    ));
                    $resp = curl_exec($curl);
                    curl_close($curl);

                    $data_codigo['return'] = 1;
                    $data_codigo['codigo'] = $codigo;
                    $_SESSION['codigo'] = $codigo;
                    $_SESSION['telefono'] = $tel;
                    echo json_encode($data_codigo);
             
                }catch(Exception $ex){
                    $data_codigo['return'] = 0;
                    $data_codigo['codigo'] = $ex->getMessage();
                    //error_log(date("d-m-Y h:i:s"). " - ".$ex->getMessage()."\n", 3, self::USER_ERROR_DIR); 

                    unset($_SESSION['codigo']);
                    unset($_SESSION['telefono']);
                    echo json_encode($data_codigo);
                }*/

            $accountSid = 'ACe8ab4310e97e582af509109e0d358ae6';
            $authToken = '04278a55999b7f0967f24b1b822ff3b8';
            $twilioNumber = '+17027103658';
            
            $telefono = "+52".$tel;
            //$codigo = $this->randomNumber(4);
            $message = "Tu codigo de acceso a centraldebases.com es el siguiente: ".$codigo;

            $client = new Client($accountSid, $authToken);

            try {
                $client->messages->create(
                    $telefono,
                    [
                        "body" => $message,
                        "from" => $twilioNumber
                    ]
                );
                
                $data_codigo['return'] = 1;
                $data_codigo['codigo'] = $codigo;
                $_SESSION['codigo'] = $codigo;
                $_SESSION['telefono'] = $tel;

                echo json_encode($data_codigo);

            } catch (TwilioException $e) {
                $data_codigo['return'] = 0;
                $data_codigo['codigo'] = $e->getMessage();
                unset($_SESSION['codigo']);
                unset($_SESSION['telefono']);

                echo json_encode($data_codigo);

            }catch(Exception $ex){
                $data_codigo['return'] = 0;
                $data_codigo['codigo'] = $ex->getMessage();
                unset($_SESSION['codigo']);
                unset($_SESSION['telefono']);

                echo json_encode($data_codigo);
            }
         }
           
        
    }

    public function randomNumber($length) {
        $result = '';

        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }

    public function servicios()
    {
        $this->load->helper('array');
        $this->load->helper('url');

        /*$arrayData['telefono'] = $this->input->post('telefono');
        $arrayData['codigo'] = $this->input->post('codigo');*/

        //$this->load->view('servicios/index');//, $arrayData);
        $this->load->view('servicios/index');
        //$this->load->view('servicios/index', $telefono);
    }

    public function activarServicio($idServicio)
    {
        $this->output->unset_template();
        try {
            $result = $this->servicios_model->activar_servicio_usuario($idServicio);
            $data_codigo['return'] = 1;
            $data_codigo['codigo'] = "";

            echo json_encode($data_codigo);
        }catch(Exception $ex){
            $data_codigo['return'] = 2;
            $data_codigo['codigo'] = $ex->getMessage();
    
            echo json_encode($data_codigo);
        }
    }

    public function operador_Servicio()
    {
        $this->output->unset_template();
        
        $idServicio =  $this->input->post('idServicio');
        $idOperador =  $this->input->post('idOperador');
        $estatus =  $this->input->post('estatus');

        try {
            $data = array(
                'idServicio'   =>  $idServicio,
                'idOperador' => $idOperador, 
                'estatus' => $estatus
            );

            $result = $this->servicios_model->operador_servicio($data);
            
            $data_codigo['return'] = 1;
            
            echo json_encode($data_codigo);

        }catch(Exception $ex){
            $data_codigo['return'] = 2;
            $data_codigo['codigo'] = $ex->getMessage();
    
            echo json_encode($data_codigo);
        }
    }


    public function grabarComentario()
    {  

        $this->form_validation->set_rules('comentario', 'Comentario', 'trim|required');
        $this->form_validation->set_message('required', 'Oops el campo %s es requerido!!');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('servicios/comentarios');
        } 
        else {
        
            $idusuario = $this->session->userdata['loggedin']['id'];
            $comentario =  $this->input->post('comentario');
            $post_data = array(
                'buzonIdUsuario'   =>  $idusuario,
                'buzonComentario' => $comentario 
            );
            
            $servicio = $this->servicios_model->grabar_comentario($post_data);
            $this->index();
        }
    }

    public function misservicios()
    {
        $this->load->helper('array');
        $this->load->helper('url');

        $_SESSION['hdOpcion'] = 2;
        if (!isset($this->session->userdata['loggedin'])) 
            $this->load->view('usuarios/login');
         else
         {
            //obtiene los servicios finalizados
            $result = $this->servicios_model->get_todos_servicios($this->session->userdata['loggedin']['id']);
            //print_r($result);
            $data['finalizados'] =$result;        

            $this->load->view('servicios/misservicios', $data);
        }
    }

    public function pedirtaxi()
    {
        $this->load->helper('array');
        $this->load->helper('url');

        $this->load->view('servicios/pedirtaxi');
    }

    public function comentarios()
    {
        $this->load->helper('array');
        $this->load->helper('url');

       
         $_SESSION['hdOpcion'] = 3;
        if (!isset($this->session->userdata['loggedin'])) 
            $this->load->view('usuarios/login');
         else
         {
            $this->load->view('servicios/comentarios');
        }
    }

    public function listado($idservicio)
    {
        $data['servicio'] =$idservicio;
        $this->load->view('servicios/finalizar', $data);
    }

    public function operadores($idServicio)
    {
        
        $this->output->unset_template();

        //$result = $this->servicios_model->get_operadores_activos();
        $result = $this->servicios_model->get_operadores_activos_no_notificados($idServicio,1);
        //print_r($result);die();
        echo json_encode($result);
    }

    public function get_operadores()
    {
        
        $this->output->unset_template();

        //$result = $this->servicios_model->get_operadores_activos();
        $result = $this->operadores_model->get_operadores();
        //print_r($result);die();
        echo json_encode($result);
    }

    public function pruebas()
    {
        $this->output->unset_template();


        print APPPATH.'vendor/autoload.php';
        print APPPATH.'../vendor/autoload.php';
        
    }

    public function getPlatform($user_agent) {
       $plataformas = array(
          'Windows 10' => 'Windows NT 10.0+',
          'Windows 8.1' => 'Windows NT 6.3+',
          'Windows 8' => 'Windows NT 6.2+',
          'Windows 7' => 'Windows NT 6.1+',
          'Windows Vista' => 'Windows NT 6.0+',
          'Windows XP' => 'Windows NT 5.1+',
          'Windows 2003' => 'Windows NT 5.2+',
          'Windows' => 'Windows otros',
          'iPhone' => 'iPhone',
          'iPad' => 'iPad',
          'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
          'Mac otros' => 'Macintosh',
          'Android' => 'Android',
          'BlackBerry' => 'BlackBerry',
          'Linux' => 'Linux',
       );
       foreach($plataformas as $plataforma=>$pattern){
          if (eregi($pattern, $user_agent))
             return $plataforma;
       }
       return 'Otras';
    }

    public function status_servicio($idServicio)
    {
        $this->output->unset_template();
        $result = $this->servicios_model->get_status_servicio($idServicio);
        if ($result == TRUE) 
            echo json_encode($result);
        else
             echo json_encode(0);
    }

    public function cancelar()
    {
        $idServicio = $this->input->post('hdServicioCancelar');

        try {
            $result = $this->servicios_model->update_servicio_usuario($idServicio, 3);
            $this->index();
        }catch(Exception $ex){
            $data_codigo['return'] = 2;
            $data_codigo['codigo'] = $ex->getMessage();
    
            $this->load->view('servicios/index', $data_codigo);
        }
    }

    public function finalizar()
    {

        $data['latitud'] = $this->input->post('hdLatitud');
        $data['longitud'] = $this->input->post('hdLongitud');
        $data['domicilio'] = $this->input->post('hdDomicilio');
        $_SESSION['hdOpcion'] = 0;
        if (!isset($this->session->userdata['loggedin'])) {
            $this->load->view('usuarios/login',$data);
        } else{

            $result = $this->servicios_model->get_servicio_usuario($this->session->userdata['loggedin']['id']);
            //echo $result; die();
            if ($result == TRUE) 
            {
                //echo $result;
                $data['encontrado'] = 1;
                $_SESSION['solicitado'] = 1;
                $result = $this->servicios_model->get_infoservicio_usuario($this->session->userdata['loggedin']['id']);
                $data['latitud'] = $result[0]->servicioLatuitudInicio;
                $data['longitud'] = $result[0]->servicioLongitudInicio; 
                $data['idServicio'] =  $result[0]->servicioId;
                $data['mensaje'] = "Ya tiene un servicio . Por favor cancele para poder solicitar uno nuevo.";
                $this->load->view('servicios/index', $data);
            }else
            {
                $_SESSION['solicitado'] = 0;
                $telefono = ($this->session->userdata['loggedin']['telefono']);
                $idusuario = ($this->session->userdata['loggedin']['id']);
                //$codigo = $this->input->post('hdCodigo');
                $latitud = $this->input->post('hdLatitud');
                $longitud = $this->input->post('hdLongitud');

                $comentario = $this->input->post('hdDomicilio');

                $post_data = array(
                    'servicioIdoperadorAuto'   =>  null,
                    'servicioIdUsuario'   =>  $idusuario,
                    'servicioStatus' => 1,
                    'servicioFechaCreacion' =>  NULL, 
                    'servicioLatuitudInicio' => $latitud, 
                    'servicioLongitudInicio' => $longitud, 
                    'servicioLatitudFin' => null, 
                    'servicioLongitudFin' => null, 
                    'servicioComentario' => $comentario, 
                    'OneSignalId' => "" 
                );
                $servicio = $this->servicios_model->insert_servicio($post_data);
                //print_r($servicio);
                if($servicio != 0)
                {
                    //notifico libres dentro de 5kms
                    $operadoreslibres = $this->obtieneNotificadoresPorDistancia($latitud, $longitud, $servicio, 1);
                    if(count($operadoreslibres)>0)
                    {
                        foreach ($operadoreslibres as $operador) {
                            if($operador['kilometros']<=2.5)
                                $this->enviar_notificacion_noreturn($operador['id_chofer'], $servicio); 
                        }
                    }

                    //notifico ocupados dentro de 10kms
                    $operadoresocupados = $this->obtieneNotificadoresPorDistancia($latitud, $longitud, $servicio, 0);

                    if(count($operadoresocupados)>0)
                    {
                        foreach ($operadoresocupados as $operador) {
                            if($operador['kilometros']<=5)
                                $this->enviar_notificacion_noreturn($operador['id_chofer'], $servicio); 
                        }
                    }

                    if(count($operadoreslibres)>0 or count($operadoresocupados)>0)
                    {
                        $data['mensaje'] = "Hemos recibido tu solicitud. Se ha notificado al operador más cercano. Espera a que se ponga en contacto contigo";
                    }
                    else
                    {
                        $data['mensaje'] = "Hemos recibido tu solicitud. Espera a que Central de Bases se comunique contigo o selecciona en el siguiente mapa el vehículo más cercano.";
                    }

                    $this->output->set_template('template');

                    $data['latitud'] = $latitud;
                    $data['longitud'] = $longitud; 
                    $data['idServicio'] =  $servicio;
                    $data['encontrado'] = 1;
                    
                    $_SESSION['solicitado'] = 1;
                    $this->load->view('servicios/index', $data);
                    //$this->load->view('servicios/success', $data);
                }
                else
                    echo "Ha ocurrido un error, por favor intentelo más tarde";
            }   
        }
    }

    public function obtieneNotificadoresPorDistancia($latitud, $longitud, $servicio, $libres)
    {
        $latitud1 = floatval($latitud);
        $longitud1 = floatval($longitud);

        $choferes = $this->servicios_model->get_operadores_activos_no_notificados($servicio, $libres);
        //print_r($choferes);
        $response=array();
        foreach($choferes as $row):
            $res=array();
            $latitud2 = floatval($row->OPLatitud);
            $longitud2 = floatval($row->OPLongitud);
            $distancia = (3958*3.1415926*sqrt(($latitud2-$latitud1)*($latitud2-$latitud1) + cos($latitud2/57.29578)*cos($latitud1/57.29578)*($longitud2-$longitud1)*($longitud2-$longitud1))/180)*1.609344;
            $kilometros = $distancia;
            $res['kilometros']=$distancia;
            $res['id_chofer']=$row->OPIdOperador;
            array_push($response, $res);
        endforeach;

        $n = count($response);
        for($i=1;$i<$n;$i++)
        {
            for($j=0;$j<$n-$i;$j++)
            {
                if($response[$j]['kilometros']>$response[$j+1]['kilometros'])
                {

                    $k1=$response[$j+1]['id_chofer'];
                    $k2=$response[$j+1]['kilometros'];

                    $response[$j+1]['id_chofer']=$response[$j]['id_chofer'];
                    $response[$j+1]['kilometros']=$response[$j]['kilometros'];

                    $response[$j]['id_chofer']=$k1;
                    $response[$j]['kilometros']=$k2;

                }
            }
        }
        return $response;
    }

    public function enviar_notificacion_noreturn($operador, $servicio)
    {
        $this->output->unset_template();
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_POST => 1, 
                CURLOPT_URL => 'http://globodi.com/centraldebases.com/central_admin/index.php/api/solicitar_operador_uno',
                CURLOPT_USERAGENT => 'cURL Request', 
                CURLOPT_POSTFIELDS => 'id_operador='.$operador.'&id_servicio='.$servicio.'&todos=1' 
            ));
            $resp = curl_exec($curl);
            curl_close($curl);
            

            $data = array(
                'idServicio'   =>  $servicio,
                'idOperador' => $operador, 
                'estatus' => 6
            );

            $result = $this->servicios_model->operador_servicio($data);

            $data_codigo['return'] = 1;
            $data_codigo['codigo'] = "El chofer ha sido notificado. Espera a que Central de Bases te contacte";
            return json_encode($data_codigo);

        }catch(Exception $ex){
            $data_codigo['return'] = 0;
            $data_codigo['codigo'] = $ex->getMessage();
            //error_log(date("d-m-Y h:i:s"). " - ".$ex->getMessage()."\n", 3, self::USER_ERROR_DIR); 
            return json_encode($data_codigo);
        }
    }

    public function enviar_notificacion($operador, $servicio, $actualizar)
    {
        $this->output->unset_template();
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_POST => 1, 
                CURLOPT_URL => 'http://globodi.com/centraldebases.com/central_admin/index.php/api/solicitar_operador_cero',
                CURLOPT_USERAGENT => 'cURL Request', 
                CURLOPT_POSTFIELDS => 'id_operador='.$operador.'&id_servicio='.$servicio.'&todos=0' 
            ));
            $resp = curl_exec($curl);
            curl_close($curl);
            

            $data = array(
                'idServicio'   =>  $servicio,
                'idOperador' => $operador, 
                'estatus' => 5
            );

            if($actualizar == "1")
            {
                $result = $this->servicios_model->update_servicio_usuario($servicio, 5, $operador);

                $result = $this->servicios_model->operador_servicio($data);
            }

            $data_codigo['return'] = 1;
            $data_codigo['codigo'] = "El chofer ha sido notificado. Espera a que Central de Bases te contacte";
            echo json_encode($data_codigo);

        }catch(Exception $ex){
            $data_codigo['return'] = 0;
            $data_codigo['codigo'] = $ex->getMessage();
            //error_log(date("d-m-Y h:i:s"). " - ".$ex->getMessage()."\n", 3, self::USER_ERROR_DIR); 
            echo json_encode($data_codigo);
        }
    }

    public function enviar_notificacion_noreturn_prueba($operador, $servicio)
    {
        $this->output->unset_template();
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_POST => 1, 
                CURLOPT_URL => 'https://www.centraldebases.com/central_admin/index.php/api/solicitar_operador_uno',
                CURLOPT_USERAGENT => 'cURL Request', 
                CURLOPT_POSTFIELDS => 'id_operador='.$operador.'&id_servicio='.$servicio.'&todos=1' 
            ));
            $resp = curl_exec($curl);
            curl_close($curl);
            

            $data = array(
                'idServicio'   =>  $servicio,
                'idOperador' => $operador, 
                'estatus' => 5
            );

            $data_codigo['return'] = 1;
            $data_codigo['codigo'] = "El chofer ha sido notificado. Espera a que Central de Bases te contacte";
            return json_encode($data_codigo);

        }catch(Exception $ex){
            $data_codigo['return'] = 0;
            $data_codigo['codigo'] = $ex->getMessage();
            //error_log(date("d-m-Y h:i:s"). " - ".$ex->getMessage()."\n", 3, self::USER_ERROR_DIR); 
            return json_encode($data_codigo);
        }
    }

    public function obtenerServicios()
    {
        $this->output->unset_template();
        $result = $this->servicios_model->get_todos_servicios($this->session->userdata['loggedin']['id']);
            //print_r($result);
        $data['servicios'] =$result;
        echo json_encode($data);
    }

    public function grabarRating()
    {  
        $idServicio = $this->input->post('hdIdServicio');
        $rating =  $this->input->post('rating');
        $comentario =  $this->input->post('comentario');

        $post_data = array(
            'idServicio'   =>  $idServicio,
            'rating' => $rating, 
            'comentario' => $comentario
        );

        //print_r($post_data);die();
        
        $servicio = $this->servicios_model->update_rating($post_data);
        redirect('usuarios/misservicios');
    }

    public function nuevo_operador_vehiculo()
    {
        $this->load->view('usuarios/registrovehiculo');
    }

    public function buscarparcial()
    {  
        $this->output->unset_template();

        $texto = $_POST['texto'];

        ///////// PAGINACION /////////
          $config = array();
          $config["base_url"] = "#";
          $config["total_rows"] = $this->operadores_model->buscarparcial($texto, 1);
          $config["per_page"] = 5;
          $config["uri_segment"] = 3;
          $config["use_page_numbers"] = TRUE;
          $config["full_tag_open"] = '<ul class="pagination" id="opera" name="opera">';
          $config["full_tag_close"] = '</ul>';
          $config["first_link"] = "Primero";
          $config["first_tag_open"] = '<li>';
          $config["first_tag_close"] = '</li>';
          $config["last_link"] = "Último";
          $config["last_tag_open"] = '<li>';
          $config["last_tag_close"] = '</li>';
          $config['next_link'] = '&gt;';
          $config["next_tag_open"] = '<li>';
          $config["next_tag_close"] = '</li>';
          $config["prev_link"] = "&lt;";
          $config["prev_tag_open"] = "<li>";
          $config["prev_tag_close"] = "</li>";
          $config["cur_tag_open"] = "<li class='active'>";
          $config["cur_tag_close"] = "</li>";
          $config["num_tag_open"] = "<li>";
          $config["num_tag_close"] = "</li>";
          $config["num_links"] = 1;

          $page = $this->uri->segment(3);
          $start = ($page - 1) * $config["per_page"];

        $result= $this->operadores_model->buscarparcial($texto,0,$config["per_page"], $start);
        $this->pagination->initialize($config);
        
        if ($result == false) {
            $strTableH = "<table class='table table-hover' id='tblOperadores' name='tblOperadores'><thead>
                    <tr>
                      <th scope='col' colspan='5' class='text-center'> NO SE ENCONTRARON COINCIDENCIAS</th>
                    </tr>
                  </thead></table>";
            
        }else {
             $strTableH = "<table class='table table-hover' id='tblOperadores' name='tblOperadores'><thead><tr><th scope='col'>Id</th><th scope='col'>Nombre</th><th scope='col'>Seleccionar</th></tr></thead><tbody>";

             foreach($result as $row) { 
                
                $strTableH = $strTableH."<tr>
                  <th scope='row'>".$row->operadorId."</th>
                  <td>".$row->operadorNombreCompleto."</td>
                  <td><input type='radio' name='rbseleccionar' id='rbseleccionar' onclick='selectoperador(\"".$row->operadorId."\")'></td></tr>";
            }
            $strTableH = $strTableH."</tbody></table>";
        }

        $str_links = $this->pagination->create_links();

         $output = array(
           'pagination_link'  => $str_links,
           'country_table'   => $strTableH, 
           'texto' => $texto
          );
          echo json_encode($output);
        ///////// END PAGINACION /////////
    }

    public function buscarparcialveh()
    {  
        $this->output->unset_template();

        $texto = $_POST['texto'];

        $result= $this->operadores_model->buscarparcialveh($texto);
        
        if ($result == false) {
            $strTableH = "<table class='table table-hover' id='tblVehiculos' name='tblVehiculos'><thead>
                    <tr>
                      <th scope='col' colspan='4' class='text-center'> NO SE ENCONTRARON COINCIDENCIAS</th>
                      <th scope='col' class='text-center'> <a href='javascript:nuevovehiculo()'><b>AGREGAR</b></a></th>
                    </tr>
                  </thead></table>";
            
        }else {
             $strTableH = "<table class='table table-hover' id='tblVehiculos' name='tblVehiculos'><thead><tr><th scope='col'>Placa</th><th scope='col'>Descripción</th><th scope='col'>Seleccionar</th></tr></thead><tbody>";

             foreach($result as $row) { 
                
                $strTableH = $strTableH."<tr>
                  <th scope='row'>".$row->autosPlacas."</th>
                  <td>".$row->autosDescripcion."</td>
                  <td><input type='radio' name='rbseleccionarVeh' id='rbseleccionarVeh' onclick='selectvehiculo(\"".$row->autosId."\")'></td></tr>";
            }
            $strTableH = $strTableH."</tbody></table>";
        }

         $output = array(
           'country_table_veh'   => $strTableH, 
           'texto' => $texto
          );
          echo json_encode($output);
        ///////// END PAGINACION /////////
    }

    public function grabarAsignacion()
    {  
        $idOperador = $this->input->post('hdIdOperador');
        $idAuto =  $this->input->post('hdIdVehiculo');

        $data = array(
            'OPIdOperador'   =>  $idOperador,
            'OPIdAuto'   =>  $idAuto,
            'OPStatust' =>  "1",
            'OPLatitud' =>  "19.25566608394677", 
            'OPLongitud' => "-103.7515141069153", 
            'token' => "",
            'imei' =>  "1"
        );

        $result = $this->operadores_model->insert_operador_auto($data);
        if ($result == FALSE) {
            $data['message_display'] = 'Ocurrió un error, por favor intente más tarde.';
            $this->load->view('usuarios/registrovehiculo', $data);
        }
        else{
            $data['message_display'] = 'Operador asignado exitosamente!';
            $this->load->view('usuarios/registrovehiculo', $data);
        }
    }

    public function cancelar_servicios()
    {
        $this->output->unset_template();
        try {
            $result = $this->servicios_model->cancelar_servicios_pendientes();
            //echo json_encode(1);
            //print_r($result);
        }catch(Exception $ex){
            echo $ex;
        }
    }

    public function liberar_servicios()
    {
        $this->output->unset_template();
        try {
            $result = $this->servicios_model->liberar_servicios();
            //echo json_encode(1);
            print_r($result);
        }catch(Exception $ex){
            echo $ex;
        }
    }

    public function existe_vehiculo($placa)
    {
        $this->output->unset_template();
        $result = $this->operadores_model->existe_auto($placa);

        if ($result)
        {
            $data['encontrado'] = 1;
            $data['vehiculo'] = $result;
        }
        else
            $data['encontrado'] = 0;

        echo json_encode($data);
    }

    public function index_base()
    {
        $this->load->view('servicios/index_base');
    }

    public function obtieneCatEstados()
    {
        $this->output->unset_template();

        $result = $this->servicios_model->cat_estados();
        $data['estados'] = $result;

        echo json_encode($data);
    }

    public function obtieneCatMunicipios($entidad)
    {
        $this->output->unset_template();

        $result = $this->servicios_model->cat_municipios($entidad);
        $data['municipios'] = $result;

        echo json_encode($data);
    }

    public function obtieneCatLocalidades($entidad, $municipio)
    {
        $this->output->unset_template();

        $result = $this->servicios_model->cat_localidades($entidad, $municipio);
        $data['localidades'] = $result;

        echo json_encode($data);
    }

    public function solicitar_base()
    {
        //print_r($_POST);die();
       
        $data['latitud'] = $this->input->post('hdLatitud');
        $data['longitud'] = $this->input->post('hdLongitud');
        $data['domicilio'] = $this->input->post('hdDomicilio');
        $data['hdNombre'] = $this->input->post('hdNombre');
        $data['hdTelefono'] = $this->input->post('hdTelefono');
        $data['hdObservaciones'] = $this->input->post('hdObservaciones');
        $data['domicilioUsuario'] = $this->input->post('hdDomicilioUsuario');
        
        if (!isset($this->session->userdata['loggedin'])) {
            $this->load->view('usuarios/login',$data);
        } 
        else
        { 
            $latitud = $this->input->post('hdLatitud');
            $longitud = $this->input->post('hdLongitud');
            $nombre = $this->input->post('hdNombre');
            $telefono = $this->input->post('hdTelefono');
            $idusuario = ($this->session->userdata['loggedin']['id']);
            $comentario = $this->input->post('hdDomicilio');
            $observaciones = $this->input->post('hdObservaciones');
            $domicilioUsuario = $this->input->post('hdDomicilioUsuario');

            $post_data = array(
                'servicioIdoperadorAuto'   =>  null,
                'servicioIdUsuario'   =>  $idusuario,
                'servicioStatus' => 1,
                'servicioFechaCreacion' =>  NULL, 
                'servicioLatuitudInicio' => $latitud, 
                'servicioLongitudInicio' => $longitud, 
                'servicioLatitudFin' => null, 
                'servicioLongitudFin' => null, 
                'servicioComentario' => $comentario, 
                'OneSignalId' => "", 
                'nombreSolicitante' => $nombre, 
                'celularSolicitante' => $telefono, 
                'observacionesSolicitante' => $observaciones, 
                'domicilioSolicitante' => ucwords(strtolower($domicilioUsuario)), 
                'base' => 1  
            );
            $servicio = $this->servicios_model->insert_servicio($post_data);
            //print_r($servicio);
            if($servicio != 0)
            {
                
                $this->notificar_operadores($latitud, $longitud, $servicio);

                $this->output->set_template('template');
                
                redirect('usuarios/index_base');
                //$this->load->view('servicios/success', $data);
            }
            else
                echo "Ha ocurrido un error, por favor intentelo más tarde"; 
        }
    }

    public function notificar_operadores($latitud, $longitud, $servicio)
    {
        //notifico libres dentro de 5kms
        $operadoreslibres = $this->obtieneNotificadoresPorDistancia($latitud, $longitud, $servicio, 1);
        if(count($operadoreslibres)>0)
        {
            foreach ($operadoreslibres as $operador) {
                if($operador['kilometros']<=2.5)
                    $this->enviar_notificacion_noreturn($operador['id_chofer'], $servicio); 
            }
        }

        //notifico ocupados dentro de 10kms
        $operadoresocupados = $this->obtieneNotificadoresPorDistancia($latitud, $longitud, $servicio, 0);

        if(count($operadoresocupados)>0)
        {
            foreach ($operadoresocupados as $operador) {
                if($operador['kilometros']<=5)
                    $this->enviar_notificacion_noreturn($operador['id_chofer'], $servicio); 
            }
        }
    }

    public function obtieneCatColonias($entidad, $municipio)
    {
        $this->output->unset_template();

        $result = $this->servicios_model->cat_colonias($entidad, $municipio);
        $data['colonias'] = $result;

        echo json_encode($data);
    }

    public function servicios_base()
    {
        if (!isset($this->session->userdata['loggedin'])) 
            $this->load->view('usuarios/login');
         else
            $this->load->view('servicios/historial_bases');
            
    }
    
    public function serviciosparcial()
    {  
        $this->output->unset_template();
        $idUsuario = $this->session->userdata['loggedin']['id'];

        ///////// PAGINACION /////////
          $config = array();
          $config["base_url"] = "#";
          $config["total_rows"] = $this->servicios_model->serviciosparcial(1,null,null,null,$idUsuario);
          $config["per_page"] = 10;
          $config["uri_segment"] = 3;
          $config["use_page_numbers"] = TRUE;
          $config["full_tag_open"] = '<ul class="pagination">';
          $config["full_tag_close"] = '</ul>';
          $config["first_link"] = "Primero";
          $config["first_tag_open"] = '<li>';
          $config["first_tag_close"] = '</li>';
          $config["last_link"] = "Último";
          $config["last_tag_open"] = '<li>';
          $config["last_tag_close"] = '</li>';
          $config['next_link'] = '&gt;';
          $config["next_tag_open"] = '<li>';
          $config["next_tag_close"] = '</li>';
          $config["prev_link"] = "&lt;";
          $config["prev_tag_open"] = "<li>";
          $config["prev_tag_close"] = "</li>";
          $config["cur_tag_open"] = "<li class='active'>";
          $config["cur_tag_close"] = "</li>";
          $config["num_tag_open"] = "<li>";
          $config["num_tag_close"] = "</li>";
          $config["num_links"] = 1;

          $page = $this->uri->segment(3);
          $start = ($page - 1) * $config["per_page"];
          //print $start;
  
        $result= $this->servicios_model->serviciosparcial(0,null,null,null,$idUsuario,$config["per_page"], $start);
        $this->pagination->initialize($config);
        
        if ($result == false) {
            $strTableH = "<table class='table table-hover table_servicios' id='tblServicios' name='tblServicios'><thead>
                    <tr>
                      <th scope='col' colspan='8' class='text-center'> NO SE ENCONTRARON SERVICIOS</th>
                    </tr>
                  </thead></table>";
            
        }else {
             $strTableH = "<table class='table table-hover table_servicios table-responsive' id='tblServicios' name='tblServicios'><thead><tr>
             <th scope='col' style='width:5%'>Id</th>
             <th scope='col' style='width:5%'>Estatus</th>
             <th scope='col' style='width:10%'>Fecha</th>
             <th scope='col' style='width:15%'>Cliente</th>
             <th scope='col' style='width:5%'>Celular</th>
             <th scope='col' style='width:20%'>Domicilio aproximado</th>
             <th scope='col' style='width:20%'>Domicilio capturado</th>
             <th scope='col' style='width:10%'>Operador</th>
             <th scope='col' style='width:10%'>Sitio</th>
             <th scope='col' style='width:5%'></th></tr></thead><tbody>";

             foreach($result as $row) { 
                switch ($row->servicioStatus) {
                    case 1:
                        $strEstatus = "Pendiente";
                        $strclass = "thNormal";
                        $activar = 0;
                        break;
                    case 2:
                       $strEstatus = "Aceptado";
                       $strclass = "thVerde";
                       $activar = 0;
                        break;
                    case 3:
                        $strEstatus = "Cancelado";
                        $strclass = "thRojo";
                        $activar = 1;
                        break;
                    case 4:
                        $strEstatus = "Rechazado";
                        $strclass = "thRojo";
                        $activar = 1;
                        break;
                }
                $date = new DateTime($row->servicioFechaCreacion);
                $operador = "";
                $sitio = "";

                if(!is_null($row->servicioIdoperadorAuto))
                {
                    $datos = $this->operadores_model->datosOperadorAuto($row->servicioIdoperadorAuto);
                    $operador = $datos[0]->OperadorNombreCompleto;
                    $sitio = $datos[0]->autosSitio." ".$datos[0]->autosNick;
                }

                $strTableH = $strTableH."<tr class='".$strclass."'>
                  <th scope='row' class='text-justify'>".$row->servicioId."</a></th>
                  <td class='text-justify'>".$strEstatus."</a></td>
                  <td class='text-justify'>".$date->format('d/m/Y H:i:s')."</td>
                  <td class='text-justify'>".$row->nombreSolicitante."</td> 
                  <td class='text-justify'>".$row->celularSolicitante."</td>
                  <td class='text-justify'>".$row->servicioComentario."</td>
                  <td class='text-justify'>".$row->domicilioSolicitante."</td>
                  <td class='text-justify'>".$operador."</td>
                  <td class='text-justify'>".$sitio."</td>";

                if($activar == 1 && $row->activar)
                    $strTableH = $strTableH."<td><button data-toggle='tooltip' data-placement='bottom' title='ACTIVAR' onclick='activar(\"".$row->servicioId."\")' >ACTIVAR</button></td>";
                else
                    $strTableH = $strTableH."<td></td>";
                 
            }
            $strTableH = $strTableH."</tr></tbody></table>";
        }

        $str_links = $this->pagination->create_links();
        
         $output = array(
           'pagination_link'  => $str_links,
           'servicios_table'   => $strTableH 
          );
          echo json_encode($output);
        ///////// END PAGINACION /////////
    }

    public function serviciosparcialfechas()
    {  
        $this->output->unset_template();
        $fechaDesde = $_POST['fechaDesde'];
        $fechaHasta = $_POST['fechaHasta'];
        $estatus = $_POST['estatus'];
        $idUsuario = $_POST['usuario'];

        $date_ini = DateTime::createFromFormat('d/m/Y', $fechaDesde);
        $date_fin = DateTime::createFromFormat('d/m/Y', $fechaHasta);
        $sfechaDesde = $date_ini->format('Y-m-d').' 00:00:00';
        $sfechaHasta = $date_fin->format('Y-m-d').' 23:59:59';

        $numrows = $this->servicios_model->serviciosparcial(1, $sfechaDesde, $sfechaHasta, $estatus, $idUsuario);
        ///////// PAGINACION /////////
         $config = array();
          $config["base_url"] = "#";
          $config["total_rows"] = $numrows;
          $config["per_page"] = 10;
          $config["uri_segment"] = 3;
          $config["use_page_numbers"] = TRUE;
          $config["full_tag_open"] = '<ul class="pagination">';
          $config["full_tag_close"] = '</ul>';
          $config["first_link"] = "Primero";
          $config["first_tag_open"] = '<li>';
          $config["first_tag_close"] = '</li>';
          $config["last_link"] = "Último";
          $config["last_tag_open"] = '<li>';
          $config["last_tag_close"] = '</li>';
          $config['next_link'] = '&gt;';
          $config["next_tag_open"] = '<li>';
          $config["next_tag_close"] = '</li>';
          $config["prev_link"] = "&lt;";
          $config["prev_tag_open"] = "<li>";
          $config["prev_tag_close"] = "</li>";
          $config["cur_tag_open"] = "<li class='active'>";
          $config["cur_tag_close"] = "</li>";
          $config["num_tag_open"] = "<li>";
          $config["num_tag_close"] = "</li>";
          $config["num_links"] = 1;

          $page = $this->uri->segment(3);
          $start = ($page - 1) * $config["per_page"];

        $result= $this->servicios_model->serviciosparcial(0, $sfechaDesde, $sfechaHasta, $estatus, $idUsuario, $config["per_page"], $start);
        $this->pagination->initialize($config);

        
        if ($result == false) {
            $strTableH = "<table class='table table-hover table_servicios table-responsive' id='tblServicios' name='tblServicios'><thead>
                    <tr>
                      <th scope='col' colspan='7' class='text-center'> NO SE ENCONTRARON SERVICIOS</th>
                    </tr>
                  </thead></table>";
            
        }else {
             $strTableH = "<table class='table table-hover table_servicios' id='tblServicios' name='tblServicios'><thead><tr>
             <th scope='col' style='width:15%'>Fecha</th>
             <th scope='col' style='width:5%'>Estatus</th>
             <th scope='col' style='width:20%'>Cliente</th>
             <th scope='col' style='width:25%'>Domicilio capturado</th>
             <th scope='col' style='width:20%'>Operador</th>
             <th scope='col' style='width:15%'>Sitio</th>
             </tr></thead>";

             foreach($result as $row) { 
                switch ($row->servicioStatus) {
                    case 1:
                        $strEstatus = "Pendiente";
                        $strclass = "thNormal";
                        $activar = 0;
                        break;
                    case 2:
                       $strEstatus = "Aceptado";
                       $strclass = "thVerde";
                       $activar = 0;
                        break;
                    case 3:
                        $strEstatus = "Cancelado";
                        $strclass = "thRojo";
                        $activar = 1;
                        break;
                    case 4:
                        $strEstatus = "Rechazado";
                        $strclass = "thRojo";
                        $activar = 1;
                        break;
                }

                $date = new DateTime($row->servicioFechaCreacion);
                $operador = "";
                $sitio = "";
                $descripcion = "";

                if(!is_null($row->servicioIdoperadorAuto))
                {
                    $datos = $this->operadores_model->datosOperadorAuto($row->servicioIdoperadorAuto);
                    $operador = $datos[0]->OperadorNombreCompleto;
                    $sitio = $datos[0]->autosSitio." ".$datos[0]->autosNick;
                    $descripcion = $datos[0]->autosDescripcion;
                }

                $strTableH = $strTableH."<tr class='".$strclass."'>
                  <td>".$date->format('d/m/Y H:i:s')."</td>
                  <td>".$strEstatus."</td>
                  <td>".$row->nombreSolicitante."</td> 
                  <td align='left'>".$row->domicilioSolicitante."</td>
                  <td>".$operador."</td>
                  <td>".$sitio."</td>";
                 
            }
            $strTableH = $strTableH."</table>";
        }

        $str_links = $this->pagination->create_links();
        
         $output = array(
           'pagination_link'  => $str_links,
           'servicios_table'   => $strTableH, 
           'total' => $numrows 
          );
          echo json_encode($output);
        ///////// END PAGINACION /////////
    }

    public function activarServicioBase($idServicio)
    {
        $this->output->unset_template();
        try {
            $result = $this->servicios_model->get_status_servicio($idServicio);

            if ($result == TRUE) 
            {
                $latitud = $result[0]->servicioLatuitudInicio;
                $longitud = $result[0]->servicioLongitudInicio;
                $result = $this->servicios_model->activar_servicio_usuario($idServicio);

                $this->notificar_operadores($latitud, $longitud, $idServicio);

                $data_codigo['return'] = 1;
                $data_codigo['codigo'] = "";

                echo json_encode($data_codigo);
            }
            else
            {
                $data_codigo['return'] = 2;
                $data_codigo['codigo'] = $ex->getMessage();
        
                echo json_encode($data_codigo);
            }  
        }catch(Exception $ex){
            $data_codigo['return'] = 2;
            $data_codigo['codigo'] = $ex->getMessage();
    
            echo json_encode($data_codigo);
        }
    }

    public function estadistico_bases()
    {
        $this->load->helper('array');
        $this->load->helper('url');

        $_SESSION['hdOpcion'] = 5;
        if (!isset($this->session->userdata['loggedin'])) 
            $this->load->view('usuarios/login');
        else
            $this->load->view('servicios/estadistico_bases');
    }

    public function estadistico()
    {
        $this->load->helper('array');
        $this->load->helper('url');

        $_SESSION['hdOpcion'] = 4;
        if (!isset($this->session->userdata['loggedin'])) 
            $this->load->view('usuarios/login');
        else
            $this->load->view('servicios/estadistico');
    }

    public function informacion_usuario()
    {
        $this->output->unset_template();
        $usuario = $_POST['usuario'];

        $result = $this->usuarios_model->informacion_usuario($usuario);

        if ($result == false) {
            $strTableH = "<table class='table table-hover' id='tblUsuarios' name='tblUsuarios'><thead>
                    <tr>
                      <th scope='col' colspan='5' class='text-center'> NO SE ENCONTRARON USUARIOS</th>
                    </tr>
                  </thead></table>";
            
        }else {
             $strTableH = "<table class='table table-hover table-responsive' id='tblUsuarios' name='tblUsuarios'><thead><tr><th scope='col'>Id</th><th scope='col'>Usuario</th><th scope='col'>Descripción</th><th scope='col'>Estado</th><th scope='col'>Zona</th><th scope='col'>Seleccionar</th></tr></thead><tbody>";

             foreach($result as $row) { 
                $zona = "";
                $estado = "";
                if(!is_null($row->municipio))
                {
                    $municipios = $this->usuarios_model->obtiene_zona($row->estado, $row->municipio);
                    foreach($municipios as $municipio) { 
                        $zona = $zona.$municipio->municipio.", ";
                        $estado = $municipio->estado;
                    }
                }

                $strTableH = $strTableH."<tr>
                  <th scope='row'>".$row->usuarioId."</th>
                  <td>".$row->usuarioNombre."</td>
                  <td>".$row->descripcionBase."</td>
                  <td>".$estado."</td>
                  <td>".$zona."</td>
                  <td><input type='radio' name='rbseleccionar' id='rbseleccionar' onclick='selectusuario(\"".$row->usuarioId."\")'></td></tr>";
            }
            $strTableH = $strTableH."</tbody></table>";
        }

         $output = array(
           'usuarios_table'   => $strTableH
          );
          echo json_encode($output);
    }

    public function impreso()
    {
        
        $fechaDesde = $_POST['hdfechaDesde'];
        $fechaHasta = $_POST['hdfechaHasta'];
        $estatus = $_POST['hdestatus'];
        $idUsuario = $_POST['hdusuario'];
        $base = $_POST['hdbase'];
        //print_r($_POST);

        if($base == "1")
        {
            $data['usuario'] = $base;
        }

        $date_ini = DateTime::createFromFormat('d/m/Y', $fechaDesde);
        $date_fin = DateTime::createFromFormat('d/m/Y', $fechaHasta);
        $sfechaDesde = $date_ini->format('Y-m-d').' 00:00:00';
        $sfechaHasta = $date_fin->format('Y-m-d').' 23:59:59';

        $result= $this->servicios_model->serviciosparcialimpreso($sfechaDesde, $sfechaHasta, $estatus, $idUsuario);

        $data['servicios'] = $result;
        $data['fechaDesde'] = $fechaDesde;
        $data['fechaHasta'] = $fechaHasta;
        $data['usuario'] = $base;
        
        $html2pdf = new Html2Pdf('L','A4','es');
        
        $html = $this->load->view('servicios/impreso', $data, true);
        $html2pdf->writeHTML($html);
        ob_end_clean();
        $html2pdf->output();
    }

    public function vermapa()
    {    
        $this->load->view('servicios/mapa');
    }

    //PITA
    public function mis_correos($id='')
    {
        if($this->input->post()){
            $this->form_validation->set_rules('correo1', 'correo1', 'trim|valid_email|required');
            $this->form_validation->set_rules('correo2', 'correo2', 'trim|valid_email|required');
            $this->form_validation->set_rules('correo3', 'correo3', 'trim|valid_email|required');

       if ($this->form_validation->run()){
        $datos = array('correo1' => $this->input->post('correo1'), 
                        'correo2' => $this->input->post('correo2'), 
                        'correo3' => $this->input->post('correo3'), 
                        'idusuario'=>$this->session->userdata['loggedin']['id'],

            );

            //Saber si ya se guardó
            $existe = $this->principal->existeRegistro('correos_user',array('idusuario'=>$this->session->userdata['loggedin']['id']));

            if($existe){
                $this->principal->update('correos_user',array('idusuario'=>$this->session->userdata['loggedin']['id']),$datos);
            }else{
                $this->principal->save('correos_user',$datos);
            }
          
          echo 1;exit();
          }else{
             $errors = array(
                  'correo1' => form_error('correo1'),
                  'correo2' => form_error('correo2'),
                  'correo3' => form_error('correo3'),
             );
            echo json_encode($errors); exit();
          }
      }
        $id = $this->session->userdata['loggedin']['id'];
        $info= $this->principal->getById('correos_user',array('idusuario'=>$id));
        
        $data['input_correo1'] = form_input('correo1',set_value('correo1',exist_obj($info,'correo1')),'class="input input100" rows="5" id="correo1" placeholder="Escríbe el correo 1" ');
        $data['input_correo2'] = form_input('correo2',set_value('correo1',exist_obj($info,'correo2')),'class="input input100" rows="5" id="correo2" placeholder="Escríbe el correo 2" ');
        $data['input_correo3'] = form_input('correo3',set_value('correo3',exist_obj($info,'correo3')),'class="input input100" rows="5" id="correo3" placeholder="Escríbe el correo 3" ');

         $_SESSION['hdOpcion'] = 3;
        if (!isset($this->session->userdata['loggedin'])) 
            $this->load->view('usuarios/login');
         else
         {
            $this->blade->set_data($data)->render('usuarios/mis_correos');
        }
    }
    //Enviar correos
    public function sendMails(){
        $id = $this->session->userdata['loggedin']['id'];
        $correos = $this->principal->getById('correos_user',array('idusuario'=>$id));
        $vista = $this->blade->render('usuarios/correos',array(),true);
        enviar_correo($correos->correo1,'¡NECESITO TU AYUDA!',$vista);
        enviar_correo($correos->correo2,'¡NECESITO TU AYUDA!',$vista);
        enviar_correo($correos->correo3,'¡NECESITO TU AYUDA!',$vista);
    }
    //MAPA 
    public function mapa(){
        $_SESSION['hdOpcion'] = 1;
        if(isset($this->session->userdata['loggedin'])){
            $this->blade->render('usuarios/mapa');
        }
        else 
             $this->load->view('usuarios/login');
        
    }
    public function getPrecioServicio(){
        $latitud = $_POST['origin_lat'];
        $latitud_destino = $_POST['destination_lat'];
        $longitud = $_POST['origin_lng'];
        $longitud_destino = $_POST['destination_lng'];

       // print_r($_POST);die();
        $response = $this->getPriceAPI($latitud,$longitud,$latitud_destino,$longitud_destino);
        
        if($response->error){
            echo json_encode(array('exito'=>false));
        }else{
             echo json_encode(array('exito'=>true,'total'=>$response->tarifa));
        }
        exit();  
    }
    public function detalleViaje(){

        $data['datos'] = $_POST;
        echo $this->blade->render('usuarios/detalle_viaje',$data);
        exit();
    }
    public function guardarViaje(){
        //print_r($_POST);die();
        $latitud = $_POST['origin_lat'];
        $latitud_destino = $_POST['destination_lat'];
        $longitud = $_POST['origin_lng'];
        $longitud_destino = $_POST['destination_lng'];

        $response = $this->getPriceAPI($latitud,$longitud,$latitud_destino,$longitud_destino);

        $precio = $response->tarifa;

        $array_servicios = array (
          'servicioIdUsuario' =>$this->session->userdata['loggedin']['id'],
          'servicioStatus' =>1,
          'servicioFechaCreacion' => date('Y-m-d H:i:s'),
          'servicioLatuitudInicio' =>$latitud ,
          'servicioLongitudInicio' =>$longitud,
          'servicioLatitudFin' =>$latitud_destino,
          'servicioLongitudFin' =>$longitud_destino,
          'servicioComentario' =>'',
          'servicioFechaFinalizacion' => null,
          'OneSignalId' => '',
          'rating' => '',
          'ratingComentario' => '',
          'nombreSolicitante' => $this->session->userdata['loggedin']['usuario'],
          'celularSolicitante' => '',
          'observacionesSolicitante' => $_POST['observaciones'],
          'domicilioSolicitante' =>'',
          'base' =>0,
          'tipo_pago' =>0,
          'tipo_auto'=>$this->session->userdata('tipo_auto'),
        );
        $this->db->insert('servicios',$array_servicios);
        $id = $this->db->insert_id();

        

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://jaimee.com.mx/central_admin/index.php/api/servicio_enviado/".$id,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Cookie: ci_session=f28ab676da5a297f1f96a99bb854c228c632cf41"
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $this->session->set_userdata('idservicio',$id);

        echo 1;exit();
    }
    function getPriceAPI($latitud='',$longitud='',$latitud_destino='',$longitud_destino=''){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://grupoave.net/panel/index.php/api_operador/calcula_tarifa",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "latitud=".$latitud."&longitud=".$longitud."&latitud_destino=".$latitud_destino."&longitud_destino=".$longitud_destino,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/x-www-form-urlencoded"
          ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);
        return $response;
    }
    //Función para la espera de chofer
    public function espera_chofer(){
        $data['datos_chofer'] = $this->usuarios_model->getDatosChofer();
        $data['existe_chofer'] = false;
        $data['url_gps'] = '';
        $data['total_servicio'] = '';
        $data['info_share'] = '';
        if(isset($data['datos_chofer'])){
            if($data['datos_chofer']->operadorId!=''){
                $data['existe_chofer'] = true;
                
                $data['info_share'] = 'Hola soy '.$this->session->userdata['loggedin']['usuario'].', quiero compartir los datos de mi viaje: Nombre Chofer: '.$data['datos_chofer']->operadorNombreCompleto.', teléfono de chofer: '.$data['datos_chofer']->operadorTelefono;
                $info_auto = $this->usuarios_model->getInfoAuto();
                if($info_auto!=''){
                    $data['url_gps'] = $info_auto->url_gps;
                    $data['total_servicio'] = $info_auto->total_servicio;
                }
                
            }
        }
        //debug_var($data);die();
        $this->blade->render('usuarios/v_espera_chofer',$data);
    }
    //Validar si ya existe el chofer
    public function validarChofer(){
        $datos_chofer = $this->usuarios_model->getDatosChofer();
        $url_gps = '';
        $total_servicio = '';
        //1.-pendiente 3.-cancelado 2.-aceptado 4.-rechazado 5.-notificado
        if($datos_chofer->servicioStatus==2 || $datos_chofer->servicioStatus==4 || $datos_chofer->servicioStatus==3){
             $this->session->set_userdata('tipo_auto','');
        }
        if($datos_chofer->servicioStatus==2){
            $info_auto = $this->usuarios_model->getInfoAuto();
            $url_gps = $info_auto->url_gps;
            $total_servicio = $info_auto->total_servicio;
        }
        echo json_encode(array('chofer'=>$datos_chofer,'estatus'=>$datos_chofer->servicioStatus,'url_gps'=>$url_gps,'total_servicio'=>$total_servicio));
        exit();
    }
    //Información de la camioneta
    public function tipo_auto(){
        $this->session->set_userdata('tipo_auto',$this->input->post('tipo_auto'));
        echo 1;exit();
        exit();
    }
    //Información de la camioneta
    public function info_camioneta(){
        $this->session->set_userdata('tipo_auto',$this->input->post('tipo_auto'));
        echo $this->blade->render('usuarios/info_camioneta');
        exit();
    }
    public function mimapa(){
        $this->blade->render('usuarios/mi_mapa');
    }
    public function redes(){
        $this->blade->render('usuarios/redes');
    }
}
?>

    