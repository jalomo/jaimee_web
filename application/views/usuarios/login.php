<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="limiter">
  <div class="container-login100">
    <div class="wrap-login100">
        <p class="text-right"><?php 
        if (isset($this->session->userdata['loggedin'])) 
        {
          echo "HOLA: ". $this->session->userdata['loggedin']['usuario'];
        }
        ?>
        </p>  
          <form id="contact-form" method="post" action="<?php echo site_url('/usuarios/proceso_login_usuario') ?>" role="form" class="login100-form">
          
            <!-- section title -->
            <div class="title text-center wow fadeInUp" data-wow-duration="500ms">
              <h2>Inicia <span >sesión</span></h2>
              <div class="border"></div>
            </div>
            <!-- /section title -->

      	
            <?php
            if (isset($logout_message)) { ?>
            <div class="text-center p-t-12">
              <span class="txterror"><?php echo $logout_message;?>
              </span>
            </div>
            <?php }
            if (isset($message_display)) { ?>
            <div class="text-center p-t-12">
              <span class="txtsuccess"><?php echo $message_display;?>
              </span>
            </div>
            <?php } 
            if (isset($error_message)) { ?>
            <div class="text-center p-t-12">
              <span class="txterror"><?php echo $error_message;?>
              </span>
            </div>
            <?php }
            ?>

            <input type="hidden" name="hdMensajeError" id="hdMensajeError" value="<?php if (isset($error_message)) echo $error_message; ?>">

            <div class="wrap-input40-login">
              <input class="input input100" type="text" id="usuario" name="usuario" placeholder="Tu Usuario">
              <span class="focus-input100"></span>
              <span class="symbol-input100">
                <i class="fa fa-user" aria-hidden="true"></i>
              </span>
            </div>
            <div class="input-group">
                 <?php echo form_error('usuario', '<div class="text-center txterror">', '</div>'); ?>
            </div>
            <div class="wrap-input40-login">
              <input class="input input100" type="password" id="password" name="password" placeholder="Contraseña">
              <span class="focus-input100"></span>
              <span class="symbol-input100">
                <i class="fa fa-lock" aria-hidden="true"></i>
              </span>
            </div>
            <div class="input-group">
                 <?php echo form_error('password', '<div class="text-center txterror">', '</div>'); ?>
            </div>
            <div style="display: none" id="codigo_error">
                <small class="text-center txterror" data-fv-validator="invalid" data-fv-for="codigo" data-fv-result="INVALID">Ha ocurrido un error, por favor intente más tarde</small>   
            </div>

            <div id="cf-submit">
                <input type="hidden" id="hdLatitud" name="hdLatitud" value="<?php if (isset($latitud)) echo $latitud ?>" />
                <input type="hidden" id="hdLongitud" name="hdLongitud" value="<?php if (isset($longitud)) echo $longitud ?>" />
                <input type="hidden" id="hdDomicilio" name="hdDomicilio" value="<?php if (isset($domicilio)) echo $domicilio ?>" />
                <input type="hidden" id="hdOpcion" name="hdOpcion" value="<?php if (isset($opcion)) echo $opcion ?>" />
                <input type="hidden" id="hdOneSignalId" name="hdOneSignalId" value="<?php if (isset($hdOneSignalId)) {echo $hdOneSignalId;}?>" />
            </div> 

            <div class="container-login25-form-btn">
              <button class="login50-form-btn" id="btn-entrar">
                Entrar
              </button>
            </div>

            <div class="title text-center wow fadeInUp p-t-12" data-wow-duration="500ms">
              <div class="border"></div>
            </div>

            <div class="container-login25-form-btn">
              <input type="button" class="registrar50-form-btn" id="btn-registrar" value="Regístrate aquí" style="cursor: pointer;"/>
            </div>
            
            <div class="text-center p-t-12">
              <span class="txt1">
                Olvidaste tu 
              </span>
              <a class="txt3" id="olvidaste" style="cursor: pointer;">
                Contraseña?
              </a>
            </div>            
        </form>
    </div>
  </div> <!-- /container -->
</div>

            <!-- Modal -->
<form id="myModal" name="myModal" class="modal wrap-modal50 validate-form2" action="<?php echo base_url() ?>index.php/Login/contrasena" method="post">
  <span class="login100-form-title">
    Indica tu número de celular
  </span>

  <div class=" wrap-input100">
    <input class="input input100" type="text" id="telefono" name="telefono" placeholder="Teléfono" maxlength="10" onkeyup="validanumero();" onkeydown="return valida(event)">
    <span class="focus-input100"></span>
    <span class="symbol-input100">
        <i class="fa fa-phone" aria-hidden="true"></i>
    </span>
  </div>
  
  <div class="container-login50-form-btn">
    <input type="button" id="btnAceptar" class="login50-form-btn" value="Aceptar" disabled/>
    <input type="button" id="btn-cancelar" class="login50-form-btn login50-form-btn-cancel" value="Cancelar"/>
  </div>
</form>


          </section>       
            <!--</div>                     
        </div>  
    </div>-->
    
    <script>

      function valida(e){
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla ==8 || tecla==9){
            return true;
        }
            
        // Patron de entrada, en este caso solo acepta numeros
        patron =/[0-9]/;
        //alert(tecla);
        tecla_final = String.fromCharCode(tecla);
        //alert(tecla_final);
        return patron.test(tecla_final);
        
        /*var key = evt.keyCode;
        alert(key);
        return (key >= 48 && key <= 57);*/

    }

    function validanumero()
    {
        if ($('#telefono').val().length < 10)
        {
            $('#btnAceptar').prop( "disabled", true );
            return false;
        }else
        {   
            $('#btnAceptar').prop( "disabled", false );
            $('#btnAceptar').focus();
                
        }
    }

      $('#olvidaste').click(function(e) 
      {
          $("#myModal").modal({backdrop: "static"});
      });


      $('#btnAceptar').click(function(e) 
      {
        var numero = $('#telefono').val().toString();
        $("#myModal").modal('hide');
        $.ajax({
            type: "POST", 
            data: { 'texto' : numero }, 
            url: "<?php echo base_url() ?>index.php/usuarios/contrasena/",
            dataType: "json",
            success: function (data) {
              if(data['return'] == 1){
                  //$("#codigo_error").css({"visibility": "disabled"});
                  
                  $("#codigo_error").prop( "style", "display:none");
                  alert('Se ha enviado un mensaje al número: '+data['codigo']);
              }
              else
              {
                  //$("#codigo_error").css({"visibility": "","color":"#FF0000"});
                  $("#codigo_error").prop( "style", "display:block");
                  //alert('return: '+data['return'])
                  alert(data['codigo']);
              }
            },
            error: function(xhr, ajaxOptions, throwError){
              //$("#codigo_error").css({"visibility": "","color":"#FF0000"});
              $("#codigo_error").prop( "style", "display:block");
              alert(throwError);
            }
          });
      });

      $('#btn-registrar').click(function(e) 
      {
        window.location="<?php echo base_url() ?>index.php/usuarios/nuevo_usuario_registro";
      });

      $('#btn-cancelar').click(function(e) 
      {
        window.location="<?php echo base_url() ?>index.php/usuarios/index";
      });
   

    </script>

  </body>
</html>