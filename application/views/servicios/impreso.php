
<page backtop="5mm" backbottom="5mm" backleft="10mm" backright="10mm"> 
    <page_header >
      
    </page_header> 
    <page_footer style="text-align: center"> 
      [[page_cu]]/[[page_nb]]
    </page_footer> 
    <?php if(isset($servicios))  { ?>
        <div style='text-align: left;'><img style='max-width: 100px; max-height: 100px;' src='<?php echo base_url(); ?>assets/images/logos/logo_10.png' /></div>
        
        <div style='text-align: center; padding-top: 15px; padding-bottom: 15px; font-size:17px;'><strong>Estadístico de servicios</strong></div>

        <div style='text-align: center; padding-top: 15px; padding-bottom: 15px; font-size:17px;'><strong>Periodo: </strong><?php echo $fechaDesde." a ".$fechaHasta; 
          if(isset($base))  {
            if($base == "0") {
        ?>
          <strong>&nbsp;&nbsp;&nbsp; Base: </strong><?php echo $base; ?>
        
        <?php 
            }
          }
        ?>
        </div>
        <table border='1' style="width: 100pt" id='tblServicios' name='tblServicios'>
            <thead>
                <tr style="text-align: center; background-color: #878787; color: #EAEAEA" >
                 <th scope='col' style='width:15%'>Fecha</th>
                 <th scope='col' style='width:5%'>Estatus</th>
                 <th scope='col' style='width:20%'>Cliente</th>
                 <th scope='col' style='width:25%'>Domicilio capturado</th>
                 <th scope='col' style='width:20%'>Operador</th>
                 <th scope='col' style='width:15%'>Sitio</th>
                </tr>
            </thead>

        <?php 
             foreach($servicios as $row) { 
                switch ($row->servicioStatus) {
                    case 1:
                        $strEstatus = "Pendiente";
                        $strclass = "thNormal";
                        $activar = 0;
                        break;
                    case 2:
                       $strEstatus = "Aceptado";
                       $strclass = "thVerde";
                       $activar = 0;
                        break;
                    case 3:
                        $strEstatus = "Cancelado";
                        $strclass = "thRojo";
                        $activar = 1;
                        break;
                    case 4:
                        $strEstatus = "Rechazado";
                        $strclass = "thRojo";
                        $activar = 1;
                        break;
                }

                $date = new DateTime($row->servicioFechaCreacion);
                $operador = "";
                $sitio = "";
                $descripcion = "";

                if(!is_null($row->servicioIdoperadorAuto))
                {
                    $datos = $this->operadores_model->datosOperadorAuto($row->servicioIdoperadorAuto);
                    $operador = $datos[0]->OperadorNombreCompleto;
                    $sitio = $datos[0]->autosSitio." ".$datos[0]->autosNick;
                    $descripcion = $datos[0]->autosDescripcion;
                }
            ?>
             <tr class='<?php echo $strclass;?>' style='font-size:10px;'>
                  <td><?php echo $date->format('d/m/Y H:i:s'); ?></td>
                  <td><?php echo $strEstatus; ?></td>
                  <td><?php echo $row->nombreSolicitante; ?></td> 
                  <td align='left'><?php echo $row->domicilioSolicitante; ?></td>
                  <td><?php echo $operador; ?></td>
                  <td><?php echo $sitio; ?></td>
              </tr>
            <?php      
            } ?>
            </table>

    <?php         
      }

    
    ?>
 </page> 