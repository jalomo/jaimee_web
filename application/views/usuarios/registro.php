<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="limiter">
  <div class="container-login100">
    <div class="wrap-login100">
    	<form id="registraform" class="login100-form" role="form" action="<?php echo site_url('/usuarios/nuevo_usuario_registro') ?>" method="post">
            <!-- section title -->
            <div class="title text-center wow fadeInUp" data-wow-duration="500ms">
                <h2>Registrar <span class="color">usuario</span></h2>
                <div class="border"></div>
            </div>
            <!-- /section title -->

            <div class="wrap-input80">
              <input class="input input100" type="text" id="usuario" name="usuario" placeholder="Usuario" onkeydown="return validaletras(event)" onblur="validausuario();" value="<?php if (isset($usuario)) {echo $usuario;}?>">
              <span class="focus-input100"></span>
              <span class="symbol-input100">
                <i class="fa fa-user" aria-hidden="true"></i>
              </span>
            </div>
            <div class="input-group">
                <?php echo form_error('usuario', '<div class="text-center p-t-12 txterror">', '</div>'); 
                if (isset($message_display)) { ?>
                    <div class="text-center p-t-12">
                      <span class="txterror"><?php echo $message_display;?>
                      </span>
                    </div>
                <?php }?>
            </div>

            <div style="display: none" id="erroruserempty">
                <small class="help-block" style="color:red" data-fv-validator="invalid" data-fv-for="codigo" data-fv-result="INVALID">Debe ingresar un nombre de usuario</small>   
            </div>

            <div style="display: none" id="erroruser">
                <small class="help-block" style="color:red" data-fv-validator="invalid" data-fv-for="codigo" data-fv-result="INVALID">Ya existe un registro con ese usuario</small>   
            </div>

            <div style="display: none" id="erroruserblanco">
                <small class="help-block" style="color:red" data-fv-validator="invalid" data-fv-for="codigo" data-fv-result="INVALID">El usuario no debe contener espacios en blanco</small>   
            </div>

            <div class="wrap-input80">
                <input class="input input100" type="password" id="password" name="password" placeholder="Contraseña" value="<?php if (isset($password)) {echo $password;}?>">
                <span class="focus-input100"></span>
                <span class="symbol-input100">
                    <i class="fa fa-lock" aria-hidden="true"></i>
                </span>
            </div>
            <div  class="input-group">
                <?php echo form_error('password', '<div class="text-center p-t-12 txterror">', '</div>'); ?>
            </div>

            <div class="wrap-input80">
                <input class="input input100" type="password" id="confirmacion" name="confirmacion" placeholder="Confirma tu contraseña">
                <span class="focus-input100"></span>
                <span class="symbol-input100">
                    <i class="fa fa-lock" aria-hidden="true"></i>
                </span>
            </div>
            <div class="input-group">
                <?php echo form_error('confirmacion', '<div class="text-center p-t-12 txterror">', '</div>'); ?>
            </div>

            <div class="wrap-input80">
                <input class="input input100" type="text" id="email" name="email" placeholder="Correo electrónico" value="<?php if (isset($email)) {echo $email;}?>">
                <span class="focus-input100"></span>
                <span class="symbol-input100">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                </span>
            </div>
            <div class="input-group">
                <?php echo form_error('email', '<div style="color:red">', '</div>'); ?>
            </div>

            <div class=" wrap-input80">
                <input class="input input100" type="text" id="telefono" name="telefono" placeholder="Teléfono"maxlength="10" onblur="validanumerousuario()" onkeyup="validanumero();"  onkeydown="return valida(event)" value="<?php if (isset($telefono)) {echo $telefono;}?>">
                <span class="focus-input100"></span>
                <span class="symbol-input100">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                </span>
            </div>
            <div style="display: none" id="errorusernumero">
                <small class="help-block" style="color:red" data-fv-validator="invalid" data-fv-for="codigo" data-fv-result="INVALID">Ya existe un registro con ese número de celular</small>    
            </div>
            
            <div class="input-group">
                <?php echo form_error('telefono', '<div class="text-center p-t-12 txterror">', '</div>'); ?>
            </div>

            <div class="wrap-input80">
                <input type="button" id="btn-solicitar" class="login50-form-btn login50-form-btn-cancel" value="Solicitar código"/>
            </div>

            <div class="wrap-input80">
              <input class="input input100" type="text" id="codigo" name="codigo" placeholder="Escribe el código que recibiste" maxlength="4" onkeydown ="return valida(event)" value="<?php if (isset($codigo)) {echo $codigo;}?>" disabled >
              <input type="hidden" id="hdCod" name="hdCod" value="" />
              <span class="focus-input100"></span>
              <span class="symbol-input100">
                <i class="fa fa-key" aria-hidden="true"></i>
              </span>
            </div>
            <div class="input-group">
                <?php echo form_error('codigo', '<div class="text-center p-t-12 txterror">', '</div>'); 
                
                if (isset($message_codigo)) { ?>
                    <div class="text-center p-t-12">
                      <span class="txterror"><?php echo $message_codigo;?>
                      </span>
                    </div>
                <?php }
                ?>
            </div>


            <div class="container-login50-form-btn">
                <button class="login50-form-btn">
                    Registrarse
                </button>
                <input type="button" id="btn-cancelar" class="login50-form-btn login50-form-btn-cancel"  value="Cancelar"/>
            </div>

            <div style="display: none;" id="codigo_error">
                <small class="help-block text-center p-t-12 txterror" style="color:red" data-fv-validator="invalid" data-fv-for="codigo" data-fv-result="INVALID">Ha ocurrido un error, por favor intente más tarde</small>   
            </div>
          </form>                   
        </div>  
    </div>
</div>

<script>
    $('#btn-cancelar').click(function(e) 
    {
        window.location="<?php echo base_url() ?>index.php/usuarios/index";
    });
    $(document).ready(function(){
        validanumero();

        /*document.getElementById('usuario').addEventListener('keydown', function(event) {
          alert(event.keyCode);
        });*/

       $('#btn-solicitar').click(function(e) 
        {
            var numero = $('#telefono').val().toString();
            var nombre = $('#usuario').val().toString();
            var password = $('#password').val().toString();
            if(nombre == '')
            {
                $('#btn-solicitar').prop( "disabled", true );
                $('#codigo').prop( "disabled", true );
                $("#usuario").focus();
                $("#erroruserempty").prop( "style", "display:block");
                return false;
            }else{
                $('#btn-solicitar').prop( "disabled", false );
                $('#codigo').prop( "disabled", false );
                $("#erroruserempty").prop( "style", "display:none");
            }
            
            $.ajax({
              type: "post",
              data: { 'usuario' : nombre, 'telefono' : numero, 'password' : password }, 
              url: "<?php echo base_url() ?>index.php/usuarios/solicitar_codigo/"+numero,
              dataType: "json",
              success: function (data) {
                //alert("SUCCESS:");
                //alert(data['return']);
                
                if(data['return'] == 1){
                    $("#hdCod").val(data['codigo']);
                    $("#codigo_error").prop( "style", "display:none");
                    alert('Se ha enviado un mensaje al número: '+numero);
                }
                else//{
                    if(data['return'] == 2){
                        $("#hdCod").val(data['codigo']);
                        $("#codigo_error").prop( "style", "display:none");
                        alert('Ya recibiste tu código antes. Consulta tus mensajes de texto.');
                    }else
                    {
                        $("#codigo_error").prop( "style", "display:block");
                        alert('return: '+data['return'])
                        alert('Codigo: '+data['codigo']);
                    }
                //}

                $('#codigo').focus();
              },
              error: function(xhr, ajaxOptions, throwError){
                $("#codigo_error").prop( "style", "display:block");
                alert(throwError);
              }
            });
        });
    });
    function valida(e){
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla ==8 || tecla==9){
            return true;
        }
            
        // Patron de entrada, en este caso solo acepta numeros
        patron =/[0-9]/;
        //alert(tecla);
        tecla_final = String.fromCharCode(tecla);
        //alert(tecla_final);
        return patron.test(tecla_final);
        
        /*var key = evt.keyCode;
        alert(key);
        return (key >= 48 && key <= 57);*/

    }

    function validaletras(e){
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla ==8 || tecla==9){
            return true;
        }
            
        // Patron de entrada, en este caso solo acepta numeros
        patron =/[A-Za-z0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);

        /*var key = evt.keyCode;
        alert(key);
        return ((key >= 48 && key <= 57) || (key >= 65 && key <= 90) || (key >= 97 && key <= 122));*/
    }
   
    function validanumero()
    {
        if ($('#telefono').val().length < 10)
        {
            $('#btn-solicitar').prop( "disabled", true );
            $('#codigo').prop( "disabled", true );
            return false;
        }else
        {   
            $('#btn-solicitar').prop( "disabled", false );
            $('#codigo').prop( "disabled", false );
            
            $('#btn-solicitar').focus();
                
        }
    }

    function validausuario(){
       //var numero = $('#telefono').val().toString();
        var nombre = $('#usuario').val().toString();
        $("#erroruserempty").prop( "style", "display:none");

        //if(numero != '' && nombre != '')
        if(nombre != '')
        {
            $.ajax({
              type: "POST",
              data: { 'usuario' : nombre},
              url: "<?php echo base_url() ?>index.php/usuarios/existe",
              dataType: "json",
              success: function (data) {
                //alert("SUCCESS:");
                //alert(data['return']);
                
                if(data['return'] == 0){
                    $('#btn-solicitar').prop( "disabled", false );
                    $('#codigo').prop( "disabled", false );
                    $("#erroruser").prop( "style", "display:none");
                    $("#erroruserblanco").css({"visibility": "hidden"});
                }
                else{
                    if(data['return'] == 3){
                        $('#btn-solicitar').prop( "disabled", true );
                        $('#codigo').prop( "disabled", true );
                        $("#erroruserblanco").css({"visibility": "","color":"#FF0000"});
                        $("#erroruser").prop( "style", "display:none");
                        $("#usuario").focus();
                        return false;
                    }
                    else
                    {
                        $('#btn-solicitar').prop( "disabled", true );
                        $('#codigo').prop( "disabled", true );
                        $("#erroruser").prop( "style", "display:block");
                        $("#erroruserblanco").prop( "style", "display:none");
                        $("#usuario").focus();
                        return false;
                    }
                }
              },
              error: function(xhr, ajaxOptions, throwError){
                $("#codigo_error").prop( "style", "display:block");
                //alert(throwError);
              }
            });
        }            
    }

    function validanumerousuario(){
       var numero = $('#telefono').val().toString();
       $("#erroruserempty").prop( "style", "display:none");

        //if(numero != '' && nombre != '')
        if(numero != '')
        {
            $.ajax({
              type: "POST",
              data: { 'numero' : numero},
              url: "<?php echo base_url() ?>index.php/usuarios/existeNumero",
              dataType: "json",
              success: function (data) {
                //alert("SUCCESS:");
                //alert(data['return']);
                
                if(data['return'] == 0){
                    $('#btn-solicitar').prop( "disabled", false );
                    $('#codigo').prop( "disabled", false );
                    $("#errorusernumero").prop( "style", "display:none");
                }
                else{
                    if(data['return'] == 3){
                        $('#btn-solicitar').prop( "disabled", true );
                        $('#codigo').prop( "disabled", true );
                        $("#errorusernumero").prop( "style", "display:none");
                        $("#telefono").focus();
                        return false;
                    }
                    else
                    {
                        $('#btn-solicitar').prop( "disabled", true );
                        $('#codigo').prop( "disabled", true );
                        $("#errorusernumero").prop( "style", "display:block");
                        $("#telefono").focus();
                        return false;
                    }
                }
              },
              error: function(xhr, ajaxOptions, throwError){
                $("#codigo_error").prop( "style", "display:block");
                //alert(throwError);
              }
            });
        }            
    }
</script>
