<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($this->session->userdata['loggedin'])) {
  $username = ($this->session->userdata['loggedin']['usuario']);
  $telefono = ($this->session->userdata['loggedin']['telefono']);
} else {
  header("location: ". site_url('usuarios/proceso_login_usuario'));
}
?>
<style type="text/css">
      html, body { height: 100%; margin: 0; padding: 0; }
      #map { height: 100%; }

      .panel-default>.panel-heading{
        color: #000;
        border-color: none;
        margin-bottom: 10px;
      }

      div #form.panel.panel-default{
        top: 10px !important;
        width: 80%;
        background-color: white;
        margin-top:80px;
        padding: 10px;
      }

      .panel{
        border: none;
        border-radius: 0;
      }

      .panel p{
        color: #7e8080;
      }

      .panel-heading{
        border: none;
        border-radius: 0;
      }

      .padding-left{
        padding-left: 5px;
      }

      .btn-primary{
        color: #fff;
        background-color: #4abea7;
        border-color: #4abea7;
      }

      .btn-primary:hover{
        background-color: #3ca28e !important;
        border-color: #4abea7 !important;
      }

      .btn-primary:active{
        background-color: #2f8977 !important;
        border-color: #4abea7 !important;
      }

      .form-inline .form-group{
        display: block;
      }

      .form-inline .form-group input{
        width: 100%;
      }
      .form-control:focus{
        background-color: #c9c9c9 !important
      }
      .input_mapa input{
        height: 35px !important;
      }
      .mt10{
        margin-top: 10px;
      }
      @media screen and (max-width: 768px){
        div #form.panel.panel-default{
          left: 0% !important;
          top: 10px !important;
          width: 100%;
          z-index: 5000 !important;
        }
        .panel-title{
          font-size: 20px !important;
          text-align: center !important;
        }
        .form-group {
            margin-bottom: 0rem !important;
        }
        .panel-heading{
          margin-bottom: 0px !important;
        }
        div #form.panel.panel-default {
          margin-top: 60px !important;
        }
      }

</style>

<div id="map"></div>

  <script type="text/javascript">
    var tipo_auto = "{{$this->session->userdata('tipo_auto')}}";
    var site_url = "{{site_url()}}";
    var global_latitud = '';
    var global_longitud = '';
    var global_formatted_address = '';
    var iconBaseA = '{{base_url()}}assets/images/logos/userA.png';
    var iconBaseB = '{{base_url()}}assets/images/logos/userB.png';
    function number_format(amount, decimals) {

        amount += ''; // por si pasan un numero en vez de un string
        amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

        decimals = decimals || 0; // por si la variable no fue fue pasada

        // si no es un numero o es igual a cero retorno el mismo cero
        if (isNaN(amount) || amount === 0) 
            return parseFloat(0).toFixed(decimals);

        // si es mayor o menor que cero retorno el valor formateado como numero
        amount = '' + amount.toFixed(decimals);

        var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;

        while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

        return amount_parts.join('.');
    }

    //Si no está el tipo de auto mandarlo a que lo elija
    if(tipo_auto=='' || tipo_auto==3){
      window.location.href = site_url+'/usuarios/index';
    }
  </script>
  

  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_gbAjK7KGkfidjIi0iLFqD39VWbhtXcs&libraries=places"></script>
  <script src="../../assets/js/custom/moment.js"></script>
  <script src="../../assets/js/custom/bundle.js"></script>
  


  </body>

</html>