<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($this->session->userdata['loggedin'])) {
  $username = ($this->session->userdata['loggedin']['usuario']);
  $telefono = ($this->session->userdata['loggedin']['telefono']);
} else {
  header("location: ". site_url('usuarios/proceso_login_usuario'));
}
?>             
<div class="limiter">
    <form id="solicitarform" class="form-horizontal" role="form">

      <!-- Google Map -->
    <div class="google-map">
      <div id="map-canvas"></div>
    </div>  
  </form>  
</div>

<!-- Modal -->
<div class="modal wrap-modal100" id="myModal" role="dialog">
  <h3 id="chofer_sitio" class="login100-form-title"></h3>

  <div class="container-login50-form-btn">
    <img class="img-responsive" src="" alt="" id="foto"/>
  </div>

  <div id="chofer_nombre" class="text-justify p-b-5 p-t-10"></div>
  <div id="chofer_telefono" class="text-justify p-b-5"></div>
  <div id="chofer_vehiculo" class="text-justify p-b-5"></div>
  <div id="chofer_tipo" class="text-justify p-b-5"></div>
  <div id="chofer_placas" class="text-justify p-b-5"></div>
  <div id="chofer_color" class="text-justify p-b-5"></div>
  
  <input type="hidden" id="hdOperador" value=""/>

  <div class="container-login50-form-btn">
    <a id="fototarjeton" class="registrar50-form-btn" >Ver tarjetón</a>
    <input type="button" id="btn-cerrar" class="login50-form-btn login50-form-btn-cancel" value="Cerrar"/>
  </div>
</div>

<div class="modal wrap-modal80" id="myModal2" role="dialog">
  <div class="container-login50-form-btn">
    <img src="" alt="" id="chofer_tarjeton"//>
  </div>
  <div class="container-login50-form-btn">
    <input type="button" id="btn-cerrarfoto" class="login50-form-btn login50-form-btn-cancel" value="Cerrar"/>
  </div>
</div>

<script type="text/javascript">
  var delay;
  var markers = [];
  var map;
  var markeruser;
  var infowindow;

  $('#btn-cerrarfoto').click(function(e) {
      //$('#img01').attr("src", $('#chofer_tarjeton').val());
      $('#myModal2').modal('hide');
    });

  $('#btn-cerrar').click(function(e) {
      //$('#img01').attr("src", $('#chofer_tarjeton').val());
      $('#myModal').modal('hide');
    });

  $('#fototarjeton').click(function(e) {
      //$('#img01').attr("src", $('#chofer_tarjeton').val());
      $('#myModal2').modal({backdrop: "static"});
    });


  function cargarImagenes(){
      var img3 = document.getElementById('chofer_tarjeton');
      img3.onerror = cargarImagenPorDefecto;
      var img4 = document.getElementById('foto');
      img4.onerror = cargarImagenPorDefecto;
    }

    function cargarImagenPorDefecto(e){
      e.target.src= "<?php echo base_url() ?>assets/images/logos/logo_10.png";
    }

  function initMap(nlat, nlng) {

    // Create a map object and specify the DOM element for display.
    map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: {lat: nlat, lng: nlng},
        zoom: 15, 
        gestureHandling: 'greedy', 
        disableDefaultUI: true
      });

      get_marcas($("#hdServicio").val());
      cargarImagenes();
    }

    function get_marcas(){
      clearMarkers() ;
      //alert('test');
      values = $.ajax({
        url : "<?php echo base_url()?>index.php/usuarios/get_operadores/",
        type : "GET",
        dataType: "json",
        async : true,
        success: function(result){
          $.each( result, function( key, value ) {
            //alert("{lat: "+value.latitud+", lng:"+value.longitud+" }" );
            var coordenadas = {lat: parseFloat(value.OPLatitud), lng:parseFloat(value.OPLongitud) };
            addMarkerWithTimeout(coordenadas, key * 200,value.autosPlacas,value.OperadorNombreCompleto,value.operadorTelefono,value.autosDescripcion,value.autosColor,value.autosSitio+' '+value.autosNick,value.OperadorImagen,value.OPIdOperador,value.OperadorImagen2,value.autosTipo,value.OPStatust);
              }); 
          }
    }).responseText;
  }

  function clearMarkers() {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(null);
    }
    markers = [];
  }

  function addMarkerWithTimeout(position,timeout,placas,nombre,telefono,vehiculo,color,sitio,foto,idOperador,foto2,tipo, status) {
      var iconBase = '<?php echo base_url() ?>assets/images/';

      if(status == 0)
      {
        if(tipo == 1)
          iconBase = iconBase + 'taxi_rojo.png';
        else
          iconBase = iconBase + 'pickup_10_rojo.png';
      }else
      {
        if(tipo == 1)
          iconBase = iconBase + 'taxi.png';
        else
          iconBase = iconBase + 'pickup_10.png';
      }

      var marker = new google.maps.Marker({
        position: position,
        map: map,
        animation: google.maps.Animation.DROP,
        icon: iconBase
      })
      markers.push(marker);

     marker.addListener('click', function() {
        if(tipo == 1)
          sTipo = 'AUTOMÓVIL';
        else 
          sTipo = 'CAMIONETA';

         //infowindow.open(map, marker);
        $("#chofer_placas").text("Placas: " + placas);
        $("#chofer_nombre").text("Chofer: " + nombre);
        $("#chofer_telefono").text("Telefono: " + telefono);
        $("#chofer_vehiculo").text("Vehículo: " + vehiculo);
        $("#chofer_color").text("Color: " + color);
        $("#chofer_sitio").text("Sitio: " + sitio);
        $("#chofer_tipo").text("Tipo: " + sTipo);
        
        $('#foto').attr("src", "<?php echo base_url() ?>central_admin/"+foto);
        $('#chofer_tarjeton').attr("src", "<?php echo base_url() ?>central_admin/"+foto2);
        //$('#chofer_tarjeton').val("<?php //echo base_url() ?>central_admin/"+foto2);
        $('#hdOperador').val(idOperador);
        $('#myModal').modal({backdrop: "static"});
      });
  }

  function getLocation() {

    if (location.protocol == 'https:'){
      // Try HTML5 geolocation.
      if (navigator.geolocation) {
        console.log("navigator.geolocation");
        navigator.geolocation.getCurrentPosition(showPosition, function(error){
            console.log("error");
            console.log(error.code);
          // El segundo parámetro es la función de error
              switch(error.code) {
                case 1:
                    alert('Por favor activa los permisos de ubicación (GPS) de manera manual en la configuración de tu dispositivo, para que se cargue el mapa correctamente y tu ubicación más exacta.');
                    break;
                case 2:
                    alert('Por favor activa los permisos de ubicación (GPS) de manera manual en la configuración de tu dispositivo, para que se cargue el mapa correctamente y tu ubicación más exacta.');
                    break;
                case 3:
                    alert('Por favor activa los permisos de ubicación (GPS) de manera manual en la configuración de tu dispositivo, para que se cargue el mapa correctamente y tu ubicación más exacta.');
                    break;
              }
              initMap(parseFloat("19.25566608394677"),parseFloat("-103.7515141069153"));
              
            }
          );
            //initMap(position.coords.latitude,position.coords.longitude); 
      } else {
        console.log("geolocate");
        //navigator.geolocation.getCurrentPosition(showPosition);
        $.ajax({
          type : 'POST',
          data: '', 
          url: "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyBnlbm19GWCdNHou3G7wSo4890x5wm7XoE", 
          success: function(result){
              initMap(result['location']['lat'],result['location']['lng']); 
                    
          }});
        }
    }else {
      console.log("geolocate");
      //navigator.geolocation.getCurrentPosition(showPosition);
      $.ajax({
        type : 'POST',
        data: '', 
        url: "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyBnlbm19GWCdNHou3G7wSo4890x5wm7XoE", 
        success: function(result){
            initMap(result['location']['lat'],result['location']['lng']); 
            console.log(result['location']['lat']);
            console.log(result['location']['lng']);
        }});
      }
  }

  function showPosition(position) {
        console.log(position.coords.latitude);
        console.log(position.coords.longitude);
        initMap(position.coords.latitude,position.coords.longitude); 
    }

  delay = window.setInterval(function() {
    get_marcas();
    console.log("get_marcas");
  }, 30000);

</script>

<script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnlbm19GWCdNHou3G7wSo4890x5wm7XoE&callback=getLocation">
</script>