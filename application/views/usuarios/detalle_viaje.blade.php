<h3>Origen:</h3>  {{$datos['origen_texto']}} <br> <br>
<h3>Destino:</h3> {{$datos['destino_texto']}}  <br> <br>
<span style="text-align: right !important;font-size: 20px;font-weight: bold">Costo aproximado:</span> 
<span style="font-size: 25px;color: blue;font-weight: bold">${{number_format($datos['precio_viaje'],2)}}
</span>


<form id="frm-confirmation">
	<div class="row">
		<div class="col-sm-12">
			<label>Indicaciones para el chofer</label>
			<textarea style="resize:none" name="observaciones" class="form-control" placeholder="ej. casa en la esquina color rojo"></textarea>
		</div>
	</div>
	<br>
	<span>***Puedes realizar el pago en efectivo o por tarjeta de crédito o débito utilizando CODI</span>
	<input type="hidden" name="origin_lat" value="{{$datos['origin_lat']}}">
	<input type="hidden" name="origin_lng" value="{{$datos['origin_lng']}}">
	<input type="hidden" name="destination_lat" value="{{$datos['destination_lat']}}">
	<input type="hidden" name="destination_lng" value="{{$datos['destination_lng']}}">
</form>
