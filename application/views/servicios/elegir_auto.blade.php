<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($this->session->userdata['loggedin'])) {
  $username = ($this->session->userdata['loggedin']['usuario']);
  $telefono = ($this->session->userdata['loggedin']['telefono']);
} else {
  header("location: ". site_url('usuarios/proceso_login_usuario'));
}
?>

<div class="limiter">
  <div class="container-login100">
    <div class="wrap-login100">
      <div class="input-group">
        <?php 
        if (isset($this->session->userdata['loggedin'])) 
        {
          ?>
          <div class="text-right wrap-input100">
            <span><?php echo "HOLA: ". $this->session->userdata['loggedin']['usuario'];?>
            </span>
          </div>
          <?php 
        }
        ?>
      </div>
            @if(count($datos_servicio)>0)
              <div class="row" style="width: 100%">
                <div class="col-sm-12">
                   <div class="input-group">
                    <h3><b><span class="btn-cancelar" style="color:red; cursor:default">Tienes un servicio en espera...</span></b></h3>
                  </div>
                </div>
              </div>
              <div class="container-login100-form-btn">
                    <input type="button" id="btn-cancelar" class="login50-form-btn login50-form-btn-cancel" value="Cancelar servicio" data-idservicio="{{(isset($idServicio))? $idServicio:'' }}">
              </div>
                
              @else
              <div class="row" style="width: 100%">
                <div class="col-sm-12">
                    <label>Seleccione el tipo de auto</label>
                    {{$drop_tipo_auto}}
                </div>
              </div>
              <div class="container-login100-form-btn">
                    <input type="button" id="btn-continuar" class="login50-form-btn login50-form-btn-cancel" value="Continuar">
              </div>

            @endif        
      </div>  
  </div>
</div>

<script type="text/javascript">
  var site_url = "{{site_url()}}";
  $("#btn-cancelar").on('click',function(e){
    e.preventDefault();
    ConfirmCustom("¿Está seguro de cancelar el servicio?", cancelarServicio,"", "Confirmar", "Cancelar");
  });
  $("#btn-continuar").on('click',function(e){
    e.preventDefault();
    var tipo_auto = $("#tipo_auto").val();
    if(tipo_auto==''){
      ErrorCustom('Es necesario seleccionar el tipo de auto');
    }else{
      if(tipo_auto==3){
        customModal(site_url+"/usuarios/info_camioneta",{"tipo_auto":tipo_auto},"POST","lg","","","","Cerrar","Detalles","modal");
      }else{
         
        ajaxJson(site_url+"/usuarios/tipo_auto",{"tipo_auto":tipo_auto},"POST","async",function(result){
          window.location.href = site_url+'/usuarios/mapa';
        }); 

      }
    }
  });

  function cancelarServicio(){
    
    var idServicio = $(this).data('idservicio');
     var url = site_url+'/usuarios/cancelar_servicio';
      ajaxJson(url,{"idServicio":idServicio},"POST","async",function(result){
        if(result){
          location.reload();
        }
      });  
  }
  

</script>