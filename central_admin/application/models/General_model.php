<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class General_model extends CI_Model{
	public function get_row($campo,$value,$tabla){
		return $this->db->where($campo,$value)->get($tabla)->row();
	}
	public function get_result($campo,$value,$tabla){
		return $this->db->where($campo,$value)->get($tabla)->result();
	}
	public function get_result_table($tabla){
		return $this->db->get($tabla)->result();
	}
	public function update($campo,$value,$tabla,$data){
		return $this->db->where($campo,$value)->update($tabla,$data);
	}
	public function insert($tabla,$data){
		$this->db->insert($tabla,$data);
		return $this->db->insert_id();
	}
	public function delete($campo,$value,$tabla){
		return $this->db->where($campo,$value)->delete($tabla);
	}

	public function get_total($tabla){
		return $this->db->count_all($tabla);
	}
	public function get_total_selection($tabla,$campo,$valor){
		return $this->db->where($campo,$valor)->count_all($tabla);
	}
	public function get_user($email,$password){
	    return $this->db->where('email',$email)
                ->where('password',$password)->get('drivers')->row();

	}

	public function get_user_phone($email,$password){
			return $this->db->where('phone',$email)
								->where('password',$password)->get('drivers')->row();

	}
	public function get_total_logged_users(){
		 			 $this->db->where('is_logged',1);
		return $this->db->count_all_results('drivers');
	}



	public function isUserExisted($email) {
					$this->db->where('email',$email);
					$query = $this->db->get('drivers');
					if($query->num_rows()>0){
						return true;
					}else{
						return false;
					}
			}

}
