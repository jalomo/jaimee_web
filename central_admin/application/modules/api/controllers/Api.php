<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Api extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mapi', '', TRUE);
        $this->load->model('General_model', '', TRUE);
        $this->load->model('Company', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'companies', 'url'));

        date_default_timezone_set('America/Mexico_City');
        header('Access-Control-Allow-Origin: *');

    }

    public function mapa_open($status){
      header('Access-Control-Allow-Origin: *');
      $data['rows'] = $this->Company->get_table('operador');
      $data['status'] = $status;

      $content = $this->load->view('api/mapa_open', $data, FALSE);
    }

    public function get_usuarios2($status, $nick=null){
      header('Access-Control-Allow-Origin: *');

      $res = $this->Company->get_operadores_status($status,$nick);//$this->Company->get_operadores();
      echo json_encode($res);
    }

    public function contacto(){
      $mensaje = $this->input->post('mensaje');
      $celular = $this->input->post('celular');
      $data['mensaje'] = $mensaje;
      $data['telefono'] = $celular;
      $data['fecha'] = date('Y-m-d H:i:s');;
      $this->Mapi->save_register('contacto',$data);
    }

    /*
    * login
    */
    public function login(){
      $email = $this->input->post('email');
      $password = $this->input->post('password');
      $token = $this->input->post('token');
      $respuesta = $this->Mapi->login($email,$password);


      if(is_object($respuesta)){
        $data_token['token'] = $token;
        $this->Mapi->actualizar_tabla('registro','id',$data_token, $respuesta->id);

        $data['error'] =0;
        $data['mensaje'] = "sin errores";
        $data['id_usuario'] = $respuesta->id;
      }else{
        $data['error'] =1;
        $data['mensaje'] = "error no puedes entrar a esta app";
        $data['id_usuario'] = 0;
      }
      echo json_encode($data);
    }

    /*
    * registro
    */
    public function registro(){
      $email = $this->input->post('email');
      $passowrd = $this->input->post('password');
      $celular = $this->input->post('celular');
      if((isset($email)) && (isset($passowrd)) && (isset($celular)) ){
        $data['email'] = $this->input->post('email');
        $data['password'] = $this->input->post('password');
        $data['celular'] = $this->input->post('celular');
        $id_usuario = $this->Mapi->save_register('registro', $data);

        $response['error'] =0;
        $response['mensaje'] = "sin errores";
        $response['id_usuario'] = $id_usuario;
      }else{
        $response['error'] =1;
        $response['mensaje'] = "error en el regitro";
        $response['id_usuario'] = 0;
      }
      echo json_encode($response);

    }

    public function mapa(){
      $id_usuario = $this->input->post('id_usuario');
      $latitud = $this->input->post('latitud');
      $longitud = $this->input->post('longitud');

      if(isset($id_usuario) && isset($latitud) && isset($longitud)){

      $resultado = $this->Mapi->mapa($id_usuario, $latitud, $longitud);
      echo json_encode($resultado);
    }else{
      $response['error'] =1;
      $response['mensaje'] = "vuelve a intentarlo";

    }

    }

    public function cotizar_servicio(){
      $latitud_inicio = $this->input->post('latitud_inicio');
    	$longitud_inicio = $this->input->post('longitud_inicio');
    	$latitud_destino = $this->input->post('latitud_destino');
    	$longitud_destino = $this->input->post('longitud_destino');
    	$token = $this->input->post('token');
    	$id_usuario = $this->input->post('id_usuario');

      $data['latitud_inicio'] = $latitud_inicio;
      $data['longitud_inicio'] = $longitud_inicio;
      $data['latitud_destino'] = $latitud_destino;
      $data['longitud_destino'] = $longitud_destino;
      $data['token'] = $token;
      $data['id_usuario'] =$id_usuario;
      $data['status'] =1;
      $data['fecha_creacion'] = date('Y-m-d H:i:s');

      $data_token['token'] = $token;
      $this->Mapi->actualizar_tabla('registro','id',$data_token, $id_usuario);

      $id_servicio = $this->Mapi->save_register('servicios', $data);

      // aqui se calculara el precio
      $data_precio['precio'] = rand(10,100);
      $this->Mapi->actualizar_tabla('servicios','id',$data_precio, $id_servicio);

      $response['error'] = 0;
      $response['mensaje'] = "cotizar servicio";
      $response['precio'] = $data_precio['precio'];
      $response['id_servicio'] = $id_servicio;
      echo json_encode($response);

    }

    public function solicitar_servicio(){
      $id_servicio = $this->input->post('id_servicio');
      if(isset($id_servicio)){

        $aux = $this->Mapi->asignar_auto($id_servicio);

        if($aux == 1){
          //$data_precio['status'] =2;
          //$this->Mapi->actualizar_tabla('servicios','id',$data_precio, $id_servicio);
          $response['error'] = 0;
          $response['mensaje'] = "buscando chofer, espere por favor";
          echo json_encode($response);
        }else{
          $data_precio['status'] =2;
          $this->Mapi->actualizar_tabla('servicios','id',$data_precio, $id_servicio);
          $response['error'] = 1;
          $response['mensaje'] = "vuelve a intentarlo";
          echo json_encode($response);
        }



      }else{
        $response['error'] = 1;
        $response['mensaje'] = "vuelve a intentarlo";
        echo json_encode($response);
      }


    }

    public function cancelar_servicio(){
      $id_servicio = $this->input->post('id_servicio');
        if(isset($id_servicio)){
        $data_precio['status'] =6;
        $this->Mapi->actualizar_tabla('servicios','id',$data_precio, $id_servicio);
        $response['error'] = 0;
        $response['mensaje'] = "servicio cancelado";
        echo json_encode($response);
      }else{
        $response['error'] = 1;
        $response['mensaje'] = "intentelo de nuevo";
        echo json_encode($response);
      }

    }

    public function test(){
      $this->load->view('api/test', '', FALSE);
    }

    public function guarda_datos(){
      $celular = $this->input->post('celular');
      $data_usuarios['nombre_completo'] = $this->input->post('dato_nombre');
      $data_usuarios['email'] = $this->input->post('dato_correo');
      $mes = $this->input->post('dato_mes');
      $dia = $this->input->post('dato_dia');
      $anio = $this->input->post('dato_anio');
      $data_usuarios['fecha_nacimiento'] = $anio."-".$mes."-".$dia;
      $data_usuarios['celular'] = $celular;

      $row_data = $this->Mapi->select_row('usuarios_datos','celular', $celular);
      if(is_object($row_data)){
        $this->Mapi->actualizar_tabla('usuarios_datos','celular',$data_usuarios, $celular);
      }else{
        $this->Mapi->save_register('usuarios_datos', $data_usuarios);
      }



      $factura_rfc['rfc'] = $this->input->post('fact_rfc');
      $factura_rfc['domicilio'] = $this->input->post('fact_domicilio');
      $factura_rfc['razon_social'] = $this->input->post('fact_razon_social');
      $factura_rfc['email'] = $this->input->post('fact_email');
      $factura_rfc['celular'] = $celular;

      $row_factura = $this->Mapi->select_row('usuarios_factura','celular', $celular);
      if(is_object($row_factura)){
        $this->Mapi->actualizar_tabla('usuarios_factura','celular',$factura_rfc, $celular);
      }else{
        $this->Mapi->save_register('usuarios_factura', $factura_rfc);
      }


      $sos['telefono1'] = $this->input->post('sos_1');
      $sos['telefono2'] = $this->input->post('sos_2');
      $sos['telefono3'] = $this->input->post('sos_3');
      $sos['celular'] = $celular;

      $row_sos = $this->Mapi->select_row('usuarios_sos','celular', $celular);
      if(is_object($row_factura)){
        $this->Mapi->actualizar_tabla('usuarios_sos','celular',$sos, $celular);
      }else{
        $this->Mapi->save_register('usuarios_sos', $sos);
      }

      $response['error'] = 0;
      echo json_encode($response);
    }

    public function guardar_servicio(){
      $id_usuario = $this->input->post('celular');
      $id_chofer = $this->input->post('id_chofer');
      $data['celular'] = $id_usuario;
      $data['id_operador'] =$id_chofer;
      $data['fecha_creacion'] =date('Y-m-d H:i:s');
      $this->Mapi->save_register('servicios', $data);

      $response['error'] = 0;
      echo json_encode($response);


    }
/*==================================================OPERADOR=============================*/
public function test_operador(){
  $this->load->view('api/operador', '', FALSE);
}

public function solicitar_operador(){
  $this->load->library('Firebase');
  $firebase = new Firebase();
  $id_operador = $this->input->post('id_operador');
  $id_servicio = $this->input->post('id_servicio');
  $status_enviar = $this->input->post('todos');// 1 superman, 0 normal
  $operador = $this->Mapi->select_row('operador_autos','OPIdOperador', $id_operador);
  $status_enviar_texto = "";
  if($status_enviar == "1"){
    $status_enviar_texto = "uno";
    $res = array();
    $res['data']['mensaje'] = "Nuevo Servicio";
    $res['data']['id_servicio'] = $id_servicio;
    $res['data']['status'] = $status_enviar_texto;
    $response3 = $firebase->send($operador->token,$res);
  }
  if($status_enviar == "0"){
    $status_enviar_texto = "cero";
    $res = array();
    $res['data']['mensaje'] = "Nuevo Servicio";
    $res['data']['id_servicio'] = $id_servicio;
    $res['data']['status'] = $status_enviar_texto;
    $response3 = $firebase->send($operador->token,$res);
  }



  $response['error'] = 0;
  echo json_encode($response);
}

public function get_usuario_imei(){
  $imei = $this->input->post('imei');
  $query = $this->Mapi->select_row('operador_autos','imei', $imei);
   $response = array();//"";
   
  if(is_object($query)){
    if($query->imei != 1){
      $operador = $this->Mapi->select_row('operador','operadorId', $query->OPIdOperador);
      $auto = $this->Mapi->select_row('autos','autosId', $query->OPIdAuto);
      $response['error'] = 2;
      $response['usuario'] = $operador->operadorUsuario;
      $response['pasword'] = $operador->operdorPassword;
      $response['placas'] = $auto->autosPlacas;
    }else{
      $response['error'] = 0;
    }

  }else{
    $response['error'] = 0;
  }


  echo json_encode($response);
}

public function solicitar_operador_uno(){
  $this->load->library('Firebase');
  $firebase = new Firebase();
  $id_operador = $this->input->post('id_operador');
  $id_servicio = $this->input->post('id_servicio');
  //$status_enviar = $this->input->post('todos');// 1 superman, 0 normal
  $operador = $this->Mapi->select_row('operador_autos','OPIdOperador', $id_operador);
    $res = array();
    $res['data']['mensaje'] = "Nuevo Servicio";
    $res['data']['id_servicio'] = $id_servicio;
    $res['data']['status'] = "uno";
    $response3 = $firebase->send($operador->token,$res);

  $response['error'] = 0;
  echo json_encode($response);
}

public function solicitar_operador_cero(){
  $this->load->library('Firebase');
  $firebase = new Firebase();
  $id_operador = $this->input->post('id_operador');
  $id_servicio = $this->input->post('id_servicio');
  //$status_enviar = $this->input->post('todos');// 1 superman, 0 normal
  $operador = $this->Mapi->select_row('operador_autos','OPIdOperador', $id_operador);

  $res = array();
  $res['data']['mensaje'] = "Nuevo Servicio";
  $res['data']['id_servicio'] = $id_servicio;
  $res['data']['status'] = "cero";
  $response3 = $firebase->send($operador->token,$res);



  $response['error'] = 0;
  echo json_encode($response);
}

public function ver_informacion_usuario(){
  $id_servicio = $this->input->post('id_servicio');
  $servicio = $this->Mapi->get_datos_servicio($id_servicio);
  if(is_object($servicio)){

    $data['error'] = 0;
    $data['usuarioNombre'] = $servicio->usuarioNombre;
    $data['usuarioId'] = $servicio->usuarioId;
    $data['usuarioCelular'] = $servicio->usuarioCelular;
    $data['servicioFechaCreacion'] = $servicio->servicioFechaCreacion;
    $data['servicioComentario'] = $servicio->servicioComentario;
    $data['servicioLatuitudInicio'] = $servicio->servicioLatuitudInicio;
    $data['servicioLongitudInicio'] = $servicio->servicioLongitudInicio;
  }else{
    $data['error'] = 1;
    $data['mensaje'] = "intente de nuevo";
  }

  echo json_encode($data);
}

public function guardar_token(){
  $id_operador = $this->input->post('id_usuario');
  $token = $this->input->post('token');
  $data['token'] = $token;
  $this->Mapi->actualizar_tabla('operador_autos','OPIdOperador',$data, $id_operador);
  $res['error'] = 0;
  echo json_encode($res);
}

public function get_usuarios(){
/*  $res = $this->Mapi->get_usuarios();
  $data['error'] = 0;
  $data['vehiculos'] = $res;
  */
  $res = $this->Mapi->get_usuarios();
  $data['error'] = 0;
  $data['vehiculos'] = $res;
  if($this->input->post('id_operador')){
    $id_operador = $this->input->post("id_operador");
    $res1 = $this->Mapi->ver_pago($id_operador);
    if(is_object($res1)){
      $data['saldo'] = $res1->pagoMontoRestante;
      if($res1->pagoMontoRestante<=0){
        $data['error'] = 2;
      }else{
        $data['error'] = 0;
      }
    }else{
      $data['error'] = 2;
      $data['saldo'] = 0;
    }

  }else{
      $data['saldo'] = 0;
  }

  echo json_encode($data);
}
public function poner_saldo(){

  /*$query = $this->db->get('operador')->result();
  foreach($query as $row){
    $data['pagoIdOperador'] = $row->operadorId;
    $data['pagoFechaCreacion'] = date('Y-m-d H:i:s');
    $data['pagoFechaInicio'] = date('Y-m-d H:i:s');
    $data['pagoFechaFin'] = '2018-12-19';
    $data['pagoEstatus'] = 1;
    $data['pagoMontoInicial'] = 5;
    $data['pagoMontoRestante'] = 5;
    $this->Mapi->save_register('pagos', $data);
    print_r($data);
    echo "<br/>";

  }*/

}
public function get_usuarios_prueba(){
  $res = $this->Mapi->get_usuarios_prueba();
  $data['error'] = 0;
  $data['vehiculos'] = $res;
  if($this->input->post('id_operador')){
    $id_operador = $this->input->post("id_operador");
    $res1 = $this->Mapi->ver_pago($id_operador);
    if(is_object($res1)){
      $data['saldo'] = $res1->pagoMontoRestante;
      if($res1->pagoMontoRestante<=0){
        $data['error'] = 2;
      }else{
        $data['error'] = 0;
      }
    }else{
      $data['error'] = 2;
      $data['saldo'] = 0;
    }

  }else{
      $data['saldo'] = 0;
  }


  echo json_encode($data);
}

public function buzon_operador(){
  $id_operador = $this->input->post('id_auto_operador');
  $texo = $this->input->post('texto');
  $data['buzonIdOperador'] = $id_operador;
  $data['buzonTexto'] = $texo;
  $data['buzonFechaCreacion'] = date('Y-m-d H:i:s');
  $this->Mapi->save_register('buzon_operador', $data);
  $res['error'] = 0;
  echo json_encode($res);
}

    public function login_operador(){
      $usuario = $this->input->post('usuario');
      $password = $this->input->post('password');
      $placas = $this->input->post('placas');
      $token = $this->input->post('token');
      $respuesta = $this->Mapi->login_operador($usuario,$password,$placas);

      if(is_object($respuesta)){

        //$imei_res = $this->Mapi->select_row('operador_autos','OPIdOperador', $respuesta->operadorId);
        $imei_res = $this->Mapi->selecionar_placa_operador($respuesta->autosId,$respuesta->operadorId);
        if($imei_res->imei == 1){
          $data['error'] =0;
          $data['mensaje'] = "sin errores";
          $data['id_usuario'] = $respuesta->operadorId;
          $data['placas'] = $respuesta->autosPlacas;
          $data['nombre'] = $respuesta->operadorNombreCompleto;
          $data['id_auto_operador'] = $respuesta->id_auto_operador;

          $data_actualizar['OPStatust'] = 0;
          $data_actualizar['imei'] = $this->input->post('imei');
          $this->Mapi->actualizar_tabla('operador_autos','OPIdOperador',$data_actualizar, $respuesta->operadorId);
          $data_actualizar2['OPStatust'] = 1;
          $data_actualizar2['token'] = $token;
          $this->Mapi->actualizar_tabla('operador_autos','	OPId',$data_actualizar2, $respuesta->id_auto_operador);
        }else{
          if($imei_res->imei == $this->input->post('imei')){
            $data['error'] =0;
            $data['mensaje'] = "sin errores";
            $data['id_usuario'] = $respuesta->operadorId;
            $data['placas'] = $respuesta->autosPlacas;
            $data['nombre'] = $respuesta->operadorNombreCompleto;
            $data['id_auto_operador'] = $respuesta->id_auto_operador;

            $data_actualizar['OPStatust'] = 0;
            //$data_actualizar['imei'] = $this->input->post('imei');
            $this->Mapi->actualizar_tabla('operador_autos','OPIdOperador',$data_actualizar, $respuesta->operadorId);
            $data_actualizar2['OPStatust'] = 1;
            $data_actualizar2['token'] = $token;
            $this->Mapi->actualizar_tabla('operador_autos','	OPId',$data_actualizar2, $respuesta->id_auto_operador);


          }else{
            $data['error'] =1;
            $data['error_msg'] = "Este usuario ya esta en uso";
            $data['id_usuario'] = 0;
          }
        }
      }else{
        $data['error'] =1;
        $data['error_msg'] = "error no puedes entrar a esta app";
        $data['id_usuario'] = 0;
      }
      echo json_encode($data);

    }

    public function inactivo(){
      $id_auto_operador = $this->input->post('id_auto_operador');

      $respuesta = $this->Mapi->select_row('auto_operador','id', $id_auto_operador);
      if(is_object($respuesta)){
        $data_actualizar['status'] = 0;
        $this->Mapi->actualizar_tabla('auto_operador','id',$data_actualizar, $id_auto_operador);
        $data['error'] =0;
        $data['mensaje'] = "usuario inactivo";
      }else{
        $data['error'] =1;
        $data['mensaje'] = "intentelo de nuevo";

      }
      echo json_encode($data);

    }

    public function cambiar_status(){
      $id_auto_operador = $this->input->post('id_auto_operador');
      $status = $this->input->post('status');
      $respuesta = $this->Mapi->select_row('operador_autos','OPIdOperador', $id_auto_operador);


      if(is_object($respuesta)){
        $data_actualizar['OPStatust'] = $status;
        $this->Mapi->actualizar_tabla('operador_autos','OPIdOperador',$data_actualizar, $id_auto_operador);
        $data['error'] =0;
        $data['mensaje'] = "usuario inactivo";
      }else{
        $data['error'] =1;
        $data['mensaje'] = "intentelo de nuevo";

      }
      echo json_encode($data);

    }

    public function mis_llamadas($id_operador){
      $response = $this->Mapi->get_llamadas_operador($id_operador);
      echo json_encode($response);
    }

    public function enviar_push_user(){


      $id_servicio = $this->input->post("id_servicio");
      $id_operador = $this->input->post("id_operador");
      $id_op = $this->Mapi->select_row('operador_autos','OPIdOperador', $id_operador);
      $data_actualizar['servicioIdoperadorAuto'] = $id_operador;//$id_op->OPId;
      $data_actualizar['servicioStatus'] = 2;
      $this->Mapi->actualizar_tabla('servicios','servicioId',$data_actualizar, $id_servicio);


      $this->Mapi->inserta_operador_servicio($id_servicio, $id_operador,2);

      $res = $this->Mapi->select_row('servicios','servicioId', $id_servicio);
      $this->send_message($res->OneSignalId,"En un momento llamara el operador");
      $response['mensaje'] = "";
      $response['error'] = 0;
    }

    public function cancelar_push_user(){

      $id_servicio = $this->input->post("id_servicio");
      $id_operador = $this->input->post("id_operador");
      $id_op = $this->Mapi->select_row('operador_autos','OPIdOperador', $id_operador);
      $data_actualizar['servicioStatus'] = 4;
      $data_actualizar['servicioIdoperadorAuto'] = $id_operador;//$id_op->OPId;
      $this->Mapi->actualizar_tabla('servicios','servicioId',$data_actualizar, $id_servicio);


      $this->Mapi->inserta_operador_servicio($id_servicio, $id_operador,4);

      $res = $this->Mapi->select_row('servicios','servicioId', $id_servicio);
      $this->send_message($res->OneSignalId,"Intente de nuevo");
      $response['mensaje'] = "";
      $response['error'] = 0;

      echo json_encode($response);
    }

  /*  public function enviar_push_user(){

      $id_servicio = $this->input->post("user_id");
      $res = $this->Mapi->select_row('servicios','servicioId', $id_servicio);
      $this->send_message($res->OneSignalId,"En un momento llamara el operador");
    }
    */

    function send_message($user_id,$message){
        //$message = "El chofer se contactara contigo ";//$this->input->post("message");
        //$user_id = $this->input->post("user_id");


       $content = array(
        "en" => "$message"
            );

        $fields = array(
            'app_id' => "84a85ef9-8cd6-4c4f-816d-7a019ad55cf3",
            //'data' => array("idServicio" => "84"),
            'include_player_ids'=> ["$user_id"], //PLAYER_ID
            'large_icon' =>"icono.png",
            //'url' => 'http://www.centraldebases.com/Central/index.php/usuarios/listado/84',
            'contents' => $content
        );

        $fields = json_encode($fields);
        //print("\nJSON sent:\n");
        //print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                   'Authorization: Basic MGE0ZTczYjUtNDM0Yy00Y2NjLWJjNGUtN2NkNzI1YmY0MzZi'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    public function llamadas_operador(){
      $id_operador = $this->input->post('id_operador');
      $id_usuario = $this->input->post('id_usuario');
      $telefono = $this->input->post('telefono');
      $servicioId = $this->input->post('servicioId');
      $data['idOperador'] = $id_operador;
      $data['idUsuario'] = $id_usuario;
      $data['telefono_usuario'] = $telefono;
      $data['id_servicio'] = $servicioId;
      $data['fecha_creacion'] = date('Y-m-d H:i:s');

      $id_aux = $this->Mapi->save_register('llamadas_operador', $data);

      $aux22 = $this->Mapi->ver_pago($id_operador);
      if(is_object($aux22)){
        $data_pago['pagoMontoRestante'] = $aux22->pagoMontoRestante - 1;
        $this->Mapi->actualizar_tabla('pagos','pagoId',$data_pago, $aux22->pagoId);
      }


      if(isset($servicioId)){
      $data_actualiza['servicioStatus'] = 2;
      $data_actualiza['servicioIdoperadorAuto'] = $id_operador;
      $this->Mapi->actualizar_tabla('servicios','servicioId',$data_actualiza, $servicioId);
      $this->Mapi->inserta_operador_servicio($servicioId, $id_operador,2);
    }

      if($id_aux>0){
        $response['error'] = 0;
      }else{
        $response['error'] = 1;
      }
      echo json_encode($response);

    }

    public function calificar_usuario(){
      $id_usuario = $this->input->post('id_usuario');
      $id_operador = $this->input->post('id_operador');
      $calificacion = $this->input->post('calificacion');
      $id_servicio = $this->input->post('id_servicio');
      $data['calificacionIdUsuario'] = $id_usuario;
      $data['calificacionIdCalificacion'] = $calificacion;
      $data['calificacionIdServicio'] = $id_servicio;
      $data['calificacionIdOperador'] = $id_operador;
      $data['calificacionFechaCreacion'] = date('Y-m-d H:i:s');
      $aux = $this->Mapi->ver_calificacion_servicio($id_operador,$id_servicio);
      if(is_object($aux)){
        $error['error'] = 1;
        $error['mensaje'] = "usuario ya calificado";
      }else{
         $this->Mapi->save_register('calificacion_usuario', $data);
         $error['error'] = 0;
         $error['mensaje'] = "Gracias por tu opinion";
      }

      echo json_encode($error);
    }

    public function mis_servicio_operador($id_iperador){
      $respuesta = $this->Mapi->mis_servicio_operador($id_iperador);
      echo json_encode($respuesta);
    }

    public function texto_calificacion(){
      $respuesta = $this->Mapi->calificacion_texto();
      echo json_encode($respuesta);
    }

    public function revisar_servicio($id_servicio){
      $id_operador = $this->input->post("id_operador");
      $saldo = $this->Mapi->ver_pago($id_operador);
      if(is_object($saldo)){
        if($saldo->pagoMontoRestante <= 0){
          $data1['status'] = 0;
          $this->Mapi->actualizar_tabla('operador_autos','OPIdOperador',$data1, $id_operador);
          $data['error'] =  2;
        }else{
          $res = $this->Mapi->select_row('servicios','servicioId', $id_servicio);
          if($res->servicioStatus == 1){
            $data['error'] =  1;
          }else{
            $data['error'] =  0;
          }
        }


      }else{

        $data1['status'] = 0;
        $this->Mapi->actualizar_tabla('operador_autos','OPIdOperador',$data1, $id_operador);
        $data['error'] =  2;

      }


      echo json_encode($data);
    }

    public function ver_saldo(){
      $id_operador = $this->input->post("id_operador");
      $res = $this->Mapi->select_row('pagos','pagoIdOperador', $id_operador);
      if(is_object($res)){
        $data['error'] = 0;
      }else{
        $data['error'] = 1;
      }

      echo json_encode($data);
    }



    public function operador_servicio_aceptado(){
      $id_operador = $this->input->post('id_operador');
      $servicioId = $this->input->post('servicioId');

      $data_actualiza['servicioStatus'] = 2;
      $data_actualiza['servicioIdoperadorAuto'] = $id_operador;
      $this->Mapi->actualizar_tabla('servicios','servicioId',$data_actualiza, $servicioId);
      $this->Mapi->inserta_operador_servicio($servicioId, $id_operador,2);
      $response['error'] = 0;
      echo json_encode($response);
    }

    public function operador_servicio_rechazado(){

      $id_operador = $this->input->post('id_operador');
      $servicioId = $this->input->post('servicioId');

      $data_actualiza['servicioStatus'] = 4;
      $data_actualiza['servicioIdoperadorAuto'] = $id_operador;
      $this->Mapi->actualizar_tabla('servicios','servicioId',$data_actualiza, $servicioId);
      $this->Mapi->inserta_operador_servicio($servicioId, $id_operador,4);
      $response['error'] = 0;
      echo json_encode($response);

    }
/*============================================ENVIAR MENSAJES====================================================*/

    public function enviar_mensajes(){
  //    error_reporting(E_ALL);
//ini_set('display_errors', '1');
      $celular = $this->input->post('celular');
      $imei = $this->input->post('imei');
      $codigo =  rand(999,9999);
    /*  $this->load->library('soap/servicioweb');
      $servicio = new servicioweb();

      $data['imei'] = "011412000099378";
      $data['de'] = "3121509261";
      $data['para'] = $celular;//"3121189964";
      $data['mensaje'] = $codigo;
      $data['fecha_msj'] = "07/01/2017";
      $data['pais'] = "MX";
      $data['pais_salida'] = "MX";
      $res = $servicio->enviar_mensaje($data);
      */

      $data_usuario['fecha_creacion'] = date('Y-m-d H:i:s');
      $data_usuario['celular'] = $celular;
      $data_usuario['codigo'] = $codigo;
      $data_usuario['imei'] = $imei;

      $codigo_test = "";

      $res2 = $this->Mapi->select_row('usuarios','celular', $celular);
      if(is_object($res2)){
          $data_cel['codigo'] = $res2->codigo;

          $codigo_test = $res2->codigo;

          $this->Mapi->actualizar_tabla('usuarios','celular',$data_cel, $celular);
      }else{

          $codigo_test = $codigo;
          $usuario = $this->Mapi->save_register('usuarios', $data_usuario);
      }



      $data_response['error'] = 0;
      $data_response['codigo'] =$codigo_test;
      echo json_encode($data_response);

    }



    public function validar_codigo(){
      $codigo = $this->input->post('codigo');
      $imei = $this->input->post('imei');
      $celular = $this->input->post('celular');
      $aux = $this->Mapi->validar_telefono_imei($celular,$imei);
      if($aux == 1){
        $res = $this->Mapi->select_row('usuarios','codigo', $codigo);
        if(is_object($res)){
          $data['error'] = 0;
          $data['celular'] = $res->celular;
          $data['codigo'] = $res->codigo;
        }else{
          $data['error'] = 1;
        }
      }else{
        $data['error'] = 1;
      }

      echo json_encode($data);

    }

    public function mapa_tel(){
      $data['error'] = 0;
    $data['vehiculos']=  $this->Mapi->mapa_tel();
      echo json_encode($data);
    }

    public function get_chat(){
      $uuid = $this->input->post('uuid');
      $id_chofer = $this->input->post('id_chofer');
      $result = $this->Mapi->get_chat($id_chofer,$uuid);
      echo json_encode($result);

    }

    public function guarda_mensaje(){
      $imei = $this->input->post('imei');
      $mensaje = $this->input->post('mensaje');
      $id_chofer = $this->input->post('id_chofer');
      $data['mensaje'] = $mensaje;
      $data['uuid'] = $imei;
      $data['id_chofer'] = $id_chofer;
      $data['status'] = 2;
      $this->Mapi->save_register('chat', $data);

      echo json_encode($data);
    }

    public function guarda_mensaje_chofer(){
      $imei = $this->input->post('imei');
      $mensaje = $this->input->post('mensaje');
      $id_chofer = $this->input->post('id_chofer');
      $data['mensaje'] = $mensaje;
      $data['uuid'] = $imei;
      $data['id_chofer'] = $id_chofer;
      $data['status'] = 1;
      $this->Mapi->save_register('chat', $data);

      echo json_encode($data);
    }


    public function actualizar_coordenadas_operador(){
      $longitud = $this->input->post('longitude');
      $latitud = $this->input->post('latitude');
      $id = $this->input->post('id_driver');
      //$token = $this->input->post('token');
      $data['OPLatitud'] = $latitud;
      $data['OPLongitud'] = $longitud;
      $data['fecha_actualizacion'] = date('Y-m-d H:i:s');
    //  $data['token'] = $token;

      $operador = $this->Mapi->select_row('operador_autos','OPIdOperador', $id);
      if(is_object($operador)){
        $status = $operador->OPStatust;
        $data1['error'] = 0;
        $data1['status'] = $status;
        $data1['fecha'] = $data['fecha_actualizacion'];
        $this->Mapi->actualizar_tabla('operador_autos','OPIdOperador',$data, $id);
        $data_coordenadas['id_operador'] = $id;
        $data_coordenadas['fecha_creacion'] = date("Y-m-d H:i:s");
        $data_coordenadas['latitud'] = $latitud;
        $data_coordenadas['longitud'] = $longitud;

        $this->Mapi->save_register('coordenadas_operador', $data_coordenadas);
      }else{
          $status = 1000;
          $data1['error'] = 1;
          $data1['status'] = $status;

      }

      echo json_encode($data1);
    }





    public function lista_chat_chofer(){
      $id_chofer = $this->input->post('id_chofer');
    $res =   $this->Mapi->lista_chat_chofer($id_chofer);
    echo json_encode($res);
    }


    public function test_send_msj(){
        $this->load->library('soap/servicioweb');
        $servicio = new servicioweb();

        $data['imei'] = "011412000099378";
        $data['de'] = "3121509261";
        $data['para'] = '3121189964';//"3121189964";
        $data['mensaje'] = 'test';
        $data['fecha_msj'] = "08/30/2017";
        $data['pais'] = "MX";
        $data['pais_salida'] = "MX";
        $res = $servicio->enviar_mensaje($data);
        var_dump($res);

    }

    public function genera_usuario(){
      $iu_usuario = $this->input->post('id_driver');
      $longitude = $this->input->post('longitude');
      $latitude = $this->input->post('latitude');


      $user = $this->General_model->get_row('usuario',$iu_usuario,'coordenadas_usuario');
      if (is_object($user)) {
        $data['latitud'] =$latitude;
        $data['longitud'] = $longitude;
        $data['fecha_actualizacion'] = date("Y-m-d H:i:s");
        $this->General_model->update('usuario',$iu_usuario,'coordenadas_usuario',$data);
      }else{
        $data['latitud'] =$latitude;
        $data['longitud'] = $longitude;
        $data['usuario'] = $iu_usuario;
        $data['fecha_actualizacion'] = date("Y-m-d H:i:s");
        $this->General_model->insert('coordenadas_usuario',$data);
      }

      $response['success'] = 1;
      echo json_encode($response);

    }


    public function buscar_usuario(){
      $rfc = $this->input->post('rfc');
      $telefono = $this->input->post('telefono');

      $response = "";

      if(isset($rfc) || isset($telefono)){

        if(isset($rfc) && isset($telefono)){
          $this->db->where('celular',$telefono);
          //$this->db->where('rfc',$rfc);
          $row = $this->db->get('usuarios_factura')->row();
          $response['error'] = 0;
          $response['mensaje'] = "Usuario encontrado";
          $response['rfc'] = $row->rfc;
          $response['razon_social'] = $row->razon_social;
          $response['email'] = $row->email;
          $response['domicilio'] = $row->domicilio;
        }else{
          if(isset($rfc)){
            $this->db->where('celular',$telefono);
            //$this->db->where('rfc',$rfc);
            $row = $this->db->get('usuarios_factura')->row();
            $response['error'] = 0;
            $response['mensaje'] = "Usuario encontrado";
            $response['rfc'] = $row->rfc;
            $response['razon_social'] = $row->razon_social;
            $response['email'] = $row->email;
            $response['domicilio'] = $row->domicilio;
          }else{
            if(isset($telefono)){
              $this->db->where('celular',$telefono);
              //$this->db->where('rfc',$rfc);
              $row = $this->db->get('usuarios_factura')->row();
              $response['error'] = 0;
              $response['mensaje'] = "Usuario encontrado";$response['rfc'] = $row->rfc;
              $response['razon_social'] = $row->razon_social;
              $response['email'] = $row->email;
              $response['domicilio'] = $row->domicilio;

            }else{
              $response['error'] = 1;
              $response['mensaje'] = "Usuario no encontrado";
            }
          }
        }
      }else{
        $response['error'] = 1;
        $response['mensaje'] = "Usuario no encontrado";
      }

      echo json_encode($response);

    }




    public function enviar_datos_factura(){
      $celular = $this->input->post('telefono');
      $email = $this->input->post('email');
      $rfc = $this->input->post('rfc');
      $domicilio = $this->input->post('domicilio');
      $razon_social = $this->input->post('razon_social');
      $id_chofer = $this->input->post('id_chofer');

      $datos['telefono'] = $celular;
      $datos['email'] = $email;
      $datos['rfc'] = $rfc;
      $datos['domicilio'] = $domicilio;
      $datos['razon_social'] = $razon_social;
      $datos['id_chofer'] = $id_chofer;
      $datos['fecha_creacion'] = date('Y-m-d H:i:s');
      $this->Mapi->save_register('solicitud_factura', $datos);
      $mensaje = "Solicitud de factura:<br/> Teléfono contacto: ".$celular."<br/>Email:".$email."<br/>".$rfc."<br/>Domicilio Fiscal:".$domicilio."<br/>Razon Social:".$razon_social."";
      $this->load->library('email');
      $this->email->from('factura@transportesselectos.com', 'Solicitud de factura');
      $this->email->to('jalomo@hotmail.es');
      $this->email->subject('Solicitud de factura');
      $this->email->message($mensaje);
      $this->email->send();

      $response['error'] = 0;
      $response['mensaje'] = "datos enviados";
      echo json_encode($response);


    }


    public function taximetro_prueba(){
      $latitud = $this->input->post('latitud');
      $longitud = $this->input->post('longitud');
      if(isset($latitud) && isset($longitud) ){
        $data['error'] = 0;
        $data['tarifa'] = rand(5, 100);
      }else{
        $data['error'] = 1;
        $data['tarifa'] = rand(5, 100);
      }

      echo json_encode($data);
    }

  }
