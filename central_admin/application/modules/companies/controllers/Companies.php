<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Companies extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Company', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'companies', 'url'));
    }


	 public function index(){

        $token = $this->input->get('token');
        if($token == "ARTQl1C83WuGieBmD04nUhfSyTAyzrIxRyNokdW"){
          $content = $this->load->view('companies/index', '', TRUE);
          $this->load->view('main/template', array('aside'=>'',
                                                         'content'=>$content,
                                                         'included_js'=>array('statics/js/modules/login.js')));


        }else{
          redirect("/");
        }

        }

	/*
	*metodo para crear usuarios
	*administradores
	*autor: jalomo <jalomo@hotmail.es>
	*/
	public function crear_admin(){

        $this->load->view('companies/registro_admin');

	}

	/**
     *metodo para guardar el registro del
	 *administrador
     *
     **/
    public function guarda_admin()
    {
        $post = $this->input->post('Registro');
        if($post)
        {
            $pass = encrypt_password($post['adminUser'],
                                     $this->config->item('encryption_key'),
                                     $post['adminPassword']);
            $post['adminPassword'] = $pass;
            //$post['adminStatus'] = 1;
			//$post['adminFecha']=date('Y-m-d');
            $id = $this->Company->save_admin($post);
            echo $id;
        }
        else{
        }
    }

	/*
	*metodo para checar el login y la contrase�a
	*/
	public function checkDataLogin()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        if(isset($username) && isset($password) && !empty($password) && !empty($username))
        {
            $pass = encrypt_password($username,
                                     $this->config->item('encryption_key'),
                                     $password);
            $total = $this->Company->count_results_users($username, $pass);
            if($total == 1)
            {
                echo "1";
            }
            else{
                echo "0";
            }
        }
        else{
            redirect('companies');
        }
    }

	/*
	*metodo para inicio de session
	*/
	public function mainView()
    {
        $post = $this->input->post('Login');
        if(isset($post) && !empty($post))
        {
            $pass = encrypt_password($post['adminUsername'],
                                     $this->config->item('encryption_key'),
                                     $post['adminPassword']);
            $dataUser = $this->Company->get_all_data_users_specific($post['adminUsername'], $pass);

            $array_session = array('C83WuGieBmD04nUhfSy'=>$dataUser->adminId);
            $this->session->set_userdata($array_session);

            if($this->session->userdata('C83WuGieBmD04nUhfSy'))
            {
				            redirect('companies/alta_operador');

            }
            else{
              redirect('companies');
            }
        }
        else{
        }
    }

	/*
	*metodo donde el usuario crea las
	*notificaciones.
	*/
	public function panel(){
	 if($this->session->userdata('C83WuGieBmD04nUhfSy'))
    {
		$menu_header = $this->load->view('companies/menu_header', '', TRUE);
        $aside = $this->load->view('companies/left_menu', '', TRUE);
        $content = $this->load->view('companies/panel', '', TRUE);
        $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                       'aside'=>$aside,
                                                       'content'=>$content,
                                                       'included_js'=>array('statics/js/libraries/form.js','statics/js/modules/notificaciones.js')));
		}
        else{
            redirect('companies');
        }

	}

  public function notificaciones(){
	 if($this->session->userdata('C83WuGieBmD04nUhfSy'))
        {
		$menu_header = $this->load->view('companies/menu_header', '', TRUE);
        $aside = $this->load->view('companies/left_menu', '', TRUE);
        $content = $this->load->view('companies/notificaciones', '', TRUE);
        $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                       'aside'=>$aside,
                                                       'content'=>$content,
                                                       'included_js'=>array('statics/js/libraries/form.js','statics/js/modules/notificaciones.js')));
		}
        else{
            redirect('companies');
        }

	}

  public function save_notificacion(){
    if($this->session->userdata('C83WuGieBmD04nUhfSy'))
    {
    $noti =   $this->input->post('notificacion');
    $operadores = $this->Company->get_table('auto_operador');

    $this->load->library('Firebase');
    $firebase = new Firebase();
      $res = array();
      $res['data']['mensaje'] = $noti;

      foreach($operadores as $chofer_row):
        $response3 = $firebase->send($chofer_row->token,$res);
      endforeach;

      $data_noti['fecha_creacion'] = date('Y-m-d H:i:s');
      $data_noti['tipo'] = 1;
      $data_noti['texto'] = $noti;
      $this->Company->save_register('notificaciones', $data_noti);

      redirect('companies/notificaciones');
    }else{
            redirect('companies');
    }

  }

  public function alta_operador(){
	 if($this->session->userdata('C83WuGieBmD04nUhfSy'))
        {
        $data['rows'] = $this->Company->get_table('operador');
		    $menu_header = $this->load->view('companies/menu_header', '', TRUE);
        $aside = $this->load->view('companies/left_menu', '', TRUE);
        $content = $this->load->view('companies/alta_operador', $data, TRUE);
        $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                       'aside'=>$aside,
                                                       'content'=>$content,
                                                       'included_js'=>array('statics/js/libraries/form.js','statics/js/modules/notificaciones.js')));
		}
        else{
            redirect('companies');
        }

	}

  public function save_operador(){
    if($this->session->userdata('C83WuGieBmD04nUhfSy'))
     {
        $data = $this->input->post('save');
        $data['fecha_creacion'] = date("Y-m-d H:i:s");
        $name = date('dmyHis').'_'.str_replace(" ", "", $_FILES['image']['name']);
        $name2 = date('dmyHsi').'_'.str_replace(" ", "", $_FILES['image2']['name']);
        $path_to_save = 'statics/fotos/';
        if(!file_exists($path_to_save)){
          mkdir($path_to_save, 0777, true);
        }
          move_uploaded_file($_FILES['image']['tmp_name'], $path_to_save.$name);
          move_uploaded_file($_FILES['image2']['tmp_name'], $path_to_save.$name2);
          $data['OperadorImagen'] = $path_to_save.$name;
          $data['OperadorImagen2'] = $path_to_save.$name2;
        $this->Company->save_register('operador', $data);
        redirect('companies/alta_operador');
   }
      else{
          redirect('companies');
      }

  }

  public function eliminar_operador($id){
    if($this->session->userdata('C83WuGieBmD04nUhfSy'))
         {
            $this->Company->eliminar_operador($id);
              redirect('companies/alta_operador/');
          }
  }

  public function editar_operador($id){
    if($this->session->userdata('C83WuGieBmD04nUhfSy'))
         {
          $data['row'] = $this->Company-> get_id_row('operador','operadorId',$id);
 		      $menu_header = $this->load->view('companies/menu_header', '', TRUE);
         $aside = $this->load->view('companies/left_menu', '', TRUE);
         $content = $this->load->view('companies/editar_operador', $data, TRUE);
         $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                        'aside'=>$aside,
                                                        'content'=>$content,
                                                        'included_js'=>array('statics/js/libraries/form.js','statics/js/modules/notificaciones.js')));
 		}
         else{
             redirect('companies');
         }

  }

  public function actualizar_operador($id){
    if($this->session->userdata('C83WuGieBmD04nUhfSy'))
         {
    $data = $this->input->post('save');
    if($_FILES['image']['tmp_name']){
    $name = date('dmyHis').'_'.str_replace(" ", "", $_FILES['image']['name']);
    $name2 = date('dmyHsi').'_'.str_replace(" ", "", $_FILES['image2']['name']);
    $path_to_save = 'statics/fotos/';
    if(!file_exists($path_to_save)){
      mkdir($path_to_save, 0777, true);
    }

      move_uploaded_file($_FILES['image2']['tmp_name'], $path_to_save.$name2);
      move_uploaded_file($_FILES['image']['tmp_name'], $path_to_save.$name);
      $data['OperadorImagen2'] = $path_to_save.$name2;
      $data['OperadorImagen'] = $path_to_save.$name;
    }





    $this->Company->actulizar_tabla('operador',$data, 'operadorId', $id);
    redirect('companies/editar_operador/'.$id);
  }
       else{
           redirect('companies');
       }
  }

  public function alta_vehiculo(){
	 if($this->session->userdata('C83WuGieBmD04nUhfSy'))
        {
        $data['rows'] = $this->Company->get_table('autos');
		    $menu_header = $this->load->view('companies/menu_header', '', TRUE);
        $aside = $this->load->view('companies/left_menu', '', TRUE);
        $content = $this->load->view('companies/alta_vehiculo', $data, TRUE);
        $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                       'aside'=>$aside,
                                                       'content'=>$content,
                                                       'included_js'=>array('statics/js/libraries/form.js','statics/js/modules/notificaciones.js')));
		}
        else{
            redirect('companies');
        }

	}

  public function save_auto(){
    if($this->session->userdata('C83WuGieBmD04nUhfSy'))
         {
    $data = $this->input->post('save');
    $data['autosFechaCreacion'] = date("Y-m-d H:i:s");

    $name = date('dmyHis').'_'.str_replace(" ", "", $_FILES['image']['name']);
    $path_to_save = 'statics/autos/';
    if(!file_exists($path_to_save)){
      mkdir($path_to_save, 0777, true);
    }
    move_uploaded_file($_FILES['image']['tmp_name'], $path_to_save.$name);
    $data['autosImagen'] = $path_to_save.$name;


    $this->Company->save_register('autos', $data);
    redirect('companies/alta_vehiculo');
  }
      else{
          redirect('companies');
      }
  }

  public function editar_vehiculo($id){
	 if($this->session->userdata('C83WuGieBmD04nUhfSy'))
        {
        $data['row'] = $this->Company->get_id_row('autos','autosId',$id);
		    $menu_header = $this->load->view('companies/menu_header', '', TRUE);
        $aside = $this->load->view('companies/left_menu', '', TRUE);
        $content = $this->load->view('companies/editar_vehiculo', $data, TRUE);
        $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                       'aside'=>$aside,
                                                       'content'=>$content,
                                                       'included_js'=>array('statics/js/libraries/form.js','statics/js/modules/notificaciones.js')));
		}
        else{
            redirect('companies');
        }

	}

  public function actualiza_vehiculo($id){
    if($this->session->userdata('C83WuGieBmD04nUhfSy'))
         {
    $data = $this->input->post('save');

    if($_FILES['image']['tmp_name']){
    $name = date('dmyHis').'_'.str_replace(" ", "", $_FILES['image']['name']);
    $path_to_save = 'statics/autos/';
    if(!file_exists($path_to_save)){
      mkdir($path_to_save, 0777, true);
    }
    move_uploaded_file($_FILES['image']['tmp_name'], $path_to_save.$name);
    $data['autosImagen'] = $path_to_save.$name;
  }


    $this->Company->actulizar_tabla('autos',$data, 'autosId', $id);
    redirect('companies/editar_vehiculo/'.$id);
  }
      else{
          redirect('companies');
      }
  }

  public function operador_auto($id_operador){
	 if($this->session->userdata('C83WuGieBmD04nUhfSy'))
        {
        $data['autos'] = $this->Company->get_table('autos');
        $data['row'] = $this->Company-> get_id_row('operador','operadorId',$id_operador);
		    $menu_header = $this->load->view('companies/menu_header', '', TRUE);
        $aside = $this->load->view('companies/left_menu', '', TRUE);
        $content = $this->load->view('companies/operador_auto', $data, TRUE);
        $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                       'aside'=>$aside,
                                                       'content'=>$content,
                                                       'included_js'=>array('statics/js/libraries/form.js','statics/js/modules/notificaciones.js')));
		}
        else{
            redirect('companies');
        }

	}

  public function actualiza_asignacion($id_operador){
    if($this->session->userdata('C83WuGieBmD04nUhfSy'))
         {
    $data = $this->input->post('save');
    $data['OPIdOperador'] = $id_operador;
    $data['OPLatitud'] = "19.3227431";
    $data['OPLongitud']  ="-103.6039185";
    $data['OPStatust'] = 0;

    $this->Company->set_auto_operador($id_operador,$data);
    redirect('companies/alta_operador/');
  }
      else{
          redirect('companies');
      }

  }

  public function registro_usuario(){
	 if($this->session->userdata('C83WuGieBmD04nUhfSy'))
        {
        $data['rows'] = $this->Company->get_table('registro');
		    $menu_header = $this->load->view('companies/menu_header', '', TRUE);
        $aside = $this->load->view('companies/left_menu', '', TRUE);
        $content = $this->load->view('companies/registro_usuario', $data, TRUE);
        $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                       'aside'=>$aside,
                                                       'content'=>$content,
                                                       'included_js'=>array('statics/js/libraries/form.js','statics/js/modules/notificaciones.js')));
		}
        else{
            redirect('companies');
        }

	}


  public function mapa($status){
   if($this->session->userdata('C83WuGieBmD04nUhfSy'))
        {
        $data['rows'] = $this->Company->get_table('operador');
        $data['status'] = $status;
        $menu_header = $this->load->view('companies/menu_header', '', TRUE);
        $aside = $this->load->view('companies/left_menu', '', TRUE);
        $content = $this->load->view('companies/mapa', $data, TRUE);
        $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                       'aside'=>$aside,
                                                       'content'=>$content,
                                                       'included_js'=>array()));

//$this->load->view('companies/mapa', '', FALSE);
    }
        else{
            redirect('companies');
        }

  }

  public function mapa_open($status){
    if($this->session->userdata('C83WuGieBmD04nUhfSy'))
         {
         $data['rows'] = $this->Company->get_table('operador');
         $data['status'] = $status;

         $content = $this->load->view('companies/mapa_open', $data, FALSE);


 //$this->load->view('companies/mapa', '', FALSE);
     }
         else{
             redirect('companies');
         }
  }

  public function get_usuarios($status, $nick=null){
    if($this->session->userdata('C83WuGieBmD04nUhfSy'))
         {
    $res = $this->Company->get_operadores_status($status,$nick);//$this->Company->get_operadores();
    echo json_encode($res);
  }
      else{
          redirect('companies');
      }
  }


  public function servicios(){
    if($this->session->userdata('C83WuGieBmD04nUhfSy'))
         {
         $data['rows'] = $this->Company->get_table('servicios');

          $this->db->from('servicios');
          $this->db->order_by("id", "desc");
          $query = $this->db->get();
          $data['rows'] =  $query->result();

 		    $menu_header = $this->load->view('companies/menu_header', '', TRUE);
         $aside = $this->load->view('companies/left_menu', '', TRUE);
         $content = $this->load->view('companies/servicios', $data, TRUE);
         $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                        'aside'=>$aside,
                                                        'content'=>$content,
                                                        'included_js'=>array('statics/js/libraries/form.js','statics/js/modules/notificaciones.js')));
 		}
         else{
             redirect('companies');
         }

  }

  public function contacto(){
    if($this->session->userdata('C83WuGieBmD04nUhfSy'))
         {


          $this->db->from('contacto');
          $this->db->order_by("contactoId", "desc");
          $query = $this->db->get();
          $data['rows'] =  $query->result();

 		    $menu_header = $this->load->view('companies/menu_header', '', TRUE);
         $aside = $this->load->view('companies/left_menu', '', TRUE);
         $content = $this->load->view('companies/contacto', $data, TRUE);
         $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                        'aside'=>$aside,
                                                        'content'=>$content,
                                                        'included_js'=>array('statics/js/libraries/form.js','statics/js/modules/notificaciones.js')));
 		}
         else{
             redirect('companies');
         }

  }

  public function facturas(){
    if($this->session->userdata('C83WuGieBmD04nUhfSy'))
         {


          $this->db->from('solicitud_factura');
          $this->db->order_by("id", "desc");
          $query = $this->db->get();
          $data['rows'] =  $query->result();

 		    $menu_header = $this->load->view('companies/menu_header', '', TRUE);
         $aside = $this->load->view('companies/left_menu', '', TRUE);
         $content = $this->load->view('companies/facturas_solicitadas', $data, TRUE);
         $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                        'aside'=>$aside,
                                                        'content'=>$content,
                                                        'included_js'=>array('statics/js/libraries/form.js','statics/js/modules/notificaciones.js')));
 		}
         else{
             redirect('companies');
         }

  }

	/**
     * Method used for close the session once logout
     * of the platform and the system can delete
     * all the values required during the session
     *
     * @return void
     **/
    public function logout()
    {
        $this->session->unset_userdata('C83WuGieBmD04nUhfSy');
        $this->session->sess_destroy();
        redirect('companies');
    }


    public function cambiar_tarifa(){
  	 if($this->session->userdata('C83WuGieBmD04nUhfSy'))
          {
          $data['tarifa'] = $this->Company->get_id_row('tarifa','id_tarifa',1);
  		    $menu_header = $this->load->view('companies/menu_header', '', TRUE);
          $aside = $this->load->view('companies/left_menu', '', TRUE);
          $content = $this->load->view('companies/tarifa', $data, TRUE);
          $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                         'aside'=>$aside,
                                                         'content'=>$content,
                                                         'included_js'=>array('statics/js/libraries/form.js','statics/js/modules/notificaciones.js')));
  		}
          else{
              redirect('companies');
          }

  	}

    public function save_tarifa(){
      if($this->session->userdata('C83WuGieBmD04nUhfSy'))
           {
      $data = $this->input->post('save');


      $this->Company->actulizar_tabla('tarifa',$data, 'id_tarifa', 1);
      redirect('companies/cambiar_tarifa/');
    }
        else{
            redirect('companies');
        }

    }


/*==========================================================TAXI TEL=======================*/

public function usuarios_registro(){
 if($this->session->userdata('C83WuGieBmD04nUhfSy'))
      {
      $data['rows'] = $this->Company->get_table('usuarios');
      $menu_header = $this->load->view('companies/menu_header', '', TRUE);
      $aside = $this->load->view('companies/left_menu', '', TRUE);
      $content = $this->load->view('companies/usuarios_registro', $data, TRUE);
      $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                     'aside'=>$aside,
                                                     'content'=>$content,
                                                     'included_js'=>array('statics/js/libraries/form.js','statics/js/modules/notificaciones.js')));
  }
      else{
          redirect('companies');
      }

}

    public function chat($id_chofer){

      if($this->session->userdata('C83WuGieBmD04nUhfSy'))
        {

          $data['rows'] = $this->Company->get_id_result_chat('chat','id_chofer',$id_chofer);//$this->Company->get_id_resultw('chat','id_chofer',$id_chofer);
          $menu_header = $this->load->view('companies/menu_header', '', TRUE);
          $aside = $this->load->view('companies/left_menu', '', TRUE);
          $content = $this->load->view('companies/chat', $data, TRUE);
          $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                         'aside'=>$aside,
                                                         'content'=>$content,
                                                         'included_js'=>array('statics/js/libraries/form.js','statics/js/modules/notificaciones.js')));

        }else{
                redirect('companies');
         }

    }


    public function ver_chat($id_chofer,$imei){

      if($this->session->userdata('C83WuGieBmD04nUhfSy'))
        {

          $data['rows'] = $this->Company->ver_chat_result($id_chofer,$imei);
          $menu_header = $this->load->view('companies/menu_header', '', TRUE);
          $aside = $this->load->view('companies/left_menu', '', TRUE);
          $content = $this->load->view('companies/ver_chat', $data, TRUE);
          $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                         'aside'=>$aside,
                                                         'content'=>$content,
                                                         'included_js'=>array('statics/js/libraries/form.js','statics/js/modules/notificaciones.js')));

        }else{
                redirect('companies');
         }

    }


    public function mapa_activos($token = null){

            if($token!=null){
              if($token == "RQBosrQowNVVjY-0etfEZtFWj9BW-sTmQUxcahMpzQf8QsalXFDFsvBgPACn6wlwsI"){
                $data['rows'] = $this->Company->get_table('operador');
                $data['status'] = 1;
                $content = $this->load->view('companies/mapa_open', $data, FALSE);
              }else{
                echo "error1";
              }

            }else{

              echo "error";
            }

    }

    public function actualizaciones_choferes(){
     if($this->session->userdata('C83WuGieBmD04nUhfSy'))
          {
          $data['rows'] = $this->Company->actualizaciones_operador();

          $menu_header = $this->load->view('companies/menu_header', '', TRUE);
          $aside = $this->load->view('companies/left_menu', '', TRUE);
          $content = $this->load->view('companies/actualizaciones_choferes', $data, TRUE);
          $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                         'aside'=>$aside,
                                                         'content'=>$content,
                                                         'included_js'=>array()));

  //$this->load->view('companies/mapa', '', FALSE);
      }
          else{
              redirect('companies');
          }

    }



    public function operadores_actividad(){
  	 if($this->session->userdata('C83WuGieBmD04nUhfSy'))
          {
          $data['rows'] = $this->Company->get_table('auto_operador');
  		    $menu_header = $this->load->view('companies/menu_header', '', TRUE);
          $aside = $this->load->view('companies/left_menu', '', TRUE);
          $content = $this->load->view('companies/operadores_actividad', $data, TRUE);
          $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                         'aside'=>$aside,
                                                         'content'=>$content,
                                                         'included_js'=>array('statics/js/libraries/form.js','statics/js/modules/notificaciones.js')));
  		}
          else{
              redirect('companies');
          }

  	}

    public function cerrar_chofer($id_chofer){
      if($this->session->userdata('C83WuGieBmD04nUhfSy'))
           {
             $data['status'] = 4;
             $this->Company->actulizar_tabla('auto_operador',$data, 'id_operador', $id_chofer);
             redirect('companies/operadores_actividad');
           }
            else{
                   redirect('companies');
            }
    }




}
