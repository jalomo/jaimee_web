
(function ($) {
    "use strict";

    
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        if( $('#pass').val()!= $('#pass2').val()) 
        {
            showValidate(input[15]);
            check=false;
        }

        if( $('#passapp').val()!= $('#passapp2').val()) 
        {
            showValidate(input[17]);
            check=false;
        }

        if( $('#cambiapass').val()!= $('#cambiapass2').val()) 
        {
            showValidate(input[15]);
            check=false;
        }

        if( $('#cambiapassapp').val()!= $('#cambiapassapp2').val()) 
        {
            showValidate(input[17]);
            check=false;
        }

        if( $('#empcambiapass').val()!= $('#empcambiapass2').val()) 
        {
            showValidate(input[3]);
            check=false;
        }

        return check;
    });


    $('.validate-form .input').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('name') == 'emailcliente')
        {
            if($(input).val() != ''){
                if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                    return false;
                }
            }
        }else{
            if($(input).attr('type') == 'email' || $(input).attr('name') == 'email' || $(input).attr('name') == 'emailf') {
                if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                    return false;
                }
            }
            else {
                if($(input).attr('name') == 'telefonocontacto' || $(input).attr('name') == 'telefonoCliente' || $(input).attr('name') == 'celular' ) {
                    if($(input).val().trim().match(/[0-9]/) == null || $(input).val().trim().length < 10) {
                        return false;
                    }
                }
                else {
                    if($(input).attr('name') == 'cp') {
                        if($(input).val().trim().match(/[0-9]/) == null  || $(input).val().trim().length < 5) {
                            return false;
                        }
                    }
                    else {
                        if($(input).attr('name') == 'prefijo')
                        {
                            if($(input).val().trim().match(/[0-9]/) == null) {
                               return false;
                            }
                        }
                        else {
                            if($(input).attr('name') != 'cambiapass' && $(input).attr('name') != 'cambiapass2' && $(input).attr('name') != 'empcambiapass' && $(input).attr('name') != 'empcambiapass2' && $(input).attr('name') != 'cambiapassapp' && $(input).attr('name') != 'cambiapassapp2')
                                if($(input).val().trim() == ''){
                                    return false;
                                }
                        }
                    }
                }
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
    var input2 = $('.validate-input2 .input100');

    $('.validate-form2').on('submit',function(){
        var check = true;

        for(var i=0; i<input2.length; i++) {
            if(validate(input2[i]) == false){
                showValidate(input2[i]);
                check=false;
            }
        }

        if( $('#pass').val()!= $('#pass2').val()) 
        {
            showValidate(input2[14]);
            check=false;
        }

        return check;
    });


    $('.validate-form2 .input').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

})(jQuery);