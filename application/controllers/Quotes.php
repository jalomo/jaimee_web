<?php

class Quotes extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

    }


    // this function will redirect to book service page
    function index()
    {
        $this->subscribe();
    }

    // this function to load service book page
    function subscribe()
    {
        $this->load->view('content/site_subscribe');
    }

    /**
     * Create New Notification
     *
     * Creates adjacency list based on item (id or slug) and shows leafs related only to current item
     *
     * @param int $user_id Current user id
     * @param string $title Current title
     *
     * @return string $response
     */
    function send_message(){
        $message = "hola";
        $user_id = "fe2d1cda-a559-4ff2-bbf1-d72f4904b785";
      

       $content = array(
        "en" => "$message"
            );

        $fields = array(
            'app_id' => "84a85ef9-8cd6-4c4f-816d-7a019ad55cf3",
            //'data' => array("idServicio" => "84"),
            'include_player_ids'=> ["$user_id"], //PLAYER_ID
            'large_icon' =>"icono.png", 
            'url' => 'http://www.centraldebases.com/Central/index.php/usuarios/listado/84',
            'contents' => $content
        );

        $fields = json_encode($fields);
        print("\nJSON sent:\n");
        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                   'Authorization: Basic MGE0ZTczYjUtNDM0Yy00Y2NjLWJjNGUtN2NkNzI1YmY0MzZi'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

}