<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($this->session->userdata['loggedin'])) {
  $username = ($this->session->userdata['loggedin']['usuario']);
  $telefono = ($this->session->userdata['loggedin']['telefono']);
} else {
  header("location: ". site_url('usuarios/proceso_login_usuario'));
}
?>             
<div class="limiter">
    <form id="solicitarform" class="form-horizontal" role="form" action="<?php echo site_url('/usuarios/solicitar_base') ?>" method="post">

        <input type="hidden" id="hdLatitud" name="hdLatitud" value="<?php if (isset($latitud)) echo $latitud ?>" />
        <input type="hidden" id="hdLongitud" name="hdLongitud" value="<?php if (isset($longitud)) echo $longitud ?>" />
        <input type="hidden" id="hdDomicilio" name="hdDomicilio" value="<?php if (isset($domicilio)) echo $domicilio ?>" />
        <input type="hidden" id="hdDomicilioUsuario" name="hdDomicilioUsuario" value="<?php if (isset($domicilioUsuario)) echo $domicilioUsuario ?>" />
        <input type="hidden" id="hdnMensaje" value="<?php if (isset($mensaje)) { echo $mensaje; } ?>"/>
        <input type="hidden" id="hdNombre" name = "hdNombre" value="<?php if (isset($hdNombre)) { echo $hdNombre; } ?>"/>
        <input type="hidden" id="hdTelefono" name="hdTelefono" value="<?php if (isset($hdTelefono)) { echo $hdTelefono; } ?>"/>
        <input type="hidden" id="hdObservaciones" name="hdObservaciones" value="<?php if (isset($hdObservaciones)) { echo $hdObservaciones; } ?>"/>

      <!-- Google Map -->
    <div class="google-map">
      <div id="map-canvas"></div>
    </div>  
  </form>  
</div>
              


<div class="modal wrap-modal100" id="myModal" role="dialog">

  <a href="<?php echo site_url('/usuarios/logout') ?>">
    <input type="button" id="btnCerrar" class="btn btn-close" value="X" title="Cerrar"/>
  </a>

  <div class="title text-center wow fadeInUp" data-wow-duration="500ms">
    <h2> <span class="color">Registrar servicio</span></h2>
    <div class="border"></div>
  </div>

  <h3>Datos del solicitante</h3>

  <div class="wrap-input80 p-t-10">
    <input class="input input100" id="nombre" type="text" class="form-control" name="nombre" placeholder="Nombre completo">
    <span class="focus-input100"></span>
    <span class="symbol-input100">
      <i class="fa fa-user" aria-hidden="true"></i>
    </span>
  </div>
  <div  class="input-group">
      <?php echo form_error('nombre', '<div class="text-center p-t-12 txterror">', '</div>'); ?>
  </div>

  <div class="wrap-input80">
    <input class="input input100" id="telefono" type="text" class="form-control" name="telefono" placeholder="CEL O TEL" maxlength="10">
    <span class="focus-input100"></span>
    <span class="symbol-input100">
      <i class="fa fa-phone" aria-hidden="true"></i>
    </span>
  </div>
  <div  class="input-group">
      <?php echo form_error('telefono', '<div class="text-center p-t-12 txterror">', '</div>'); ?>
  </div>

  <h3>Domicilio</h3>

  <div class="wrap-input80 p-t-10">
    <select class="input input100" id="cmbEstado" name = "cmbEstado" onchange="obtieneMunicipio()" disabled>
    </select>

    <span class="focus-input100"></span>
    <span class="symbol-input100">
        <i class="fa fa-filter" aria-hidden="true"></i>
    </span>
  </div>
  <div  class="input-group">
      <?php echo form_error('cmbEstado', '<div class="text-center p-t-12 txterror">', '</div>'); ?>
  </div>

  <div class="wrap-input80">
    <select class="input input100" id="cmbMunicipio" name = "cmbMunicipio" onchange="obtieneLocalidad()">
    </select>

    <span class="focus-input100"></span>
    <span class="symbol-input100">
        <i class="fa fa-filter" aria-hidden="true"></i>
    </span>
  </div>
  <div  class="input-group">
      <?php echo form_error('cmbMunicipio', '<div class="text-center p-t-12 txterror">', '</div>'); ?>
  </div>

  <div class="wrap-input80">
    <select class="input input100" id="cmbLocalidad" name = "cmbLocalidad" onchange="obtieneColonia()">
    </select>

    <span class="focus-input100"></span>
    <span class="symbol-input100">
        <i class="fa fa-filter" aria-hidden="true"></i>
    </span>
  </div>
  <div  class="input-group">
      <?php echo form_error('cmbLocalidad', '<div class="text-center p-t-12 txterror">', '</div>'); ?>
  </div>

  <div class="wrap-input80">
    <select class="input input100" id="cmbColonia" name = "cmbColonia">
      <option value="0" disabled selected hidden>COLONIA</option>
    </select>

    <span class="focus-input100"></span>
    <span class="symbol-input100">
        <i class="fa fa-filter" aria-hidden="true"></i>
    </span>
  </div>
  <div  class="input-group">
      <?php echo form_error('cmbColonia', '<div class="text-center p-t-12 txterror">', '</div>'); ?>
  </div>

  <div class="wrap-input80">
    <input class="input input100" id="calle" type="text" class="form-control" name="calle" placeholder="CALLE">

    <span class="focus-input100"></span>
    <span class="symbol-input100">
        <i class="fa fa-filter" aria-hidden="true"></i>
    </span>
  </div>
  <div  class="input-group">
      <?php echo form_error('calle', '<div class="text-center p-t-12 txterror">', '</div>'); ?>
  </div>

  <div class="wrap-input80">
    <input class="input input100" id="numero" type="text" class="form-control" name="numero" placeholder="NÚMERO EXT. NÚMERO INT.">

    <span class="focus-input100"></span>
    <span class="symbol-input100">
        <i class="fa fa-filter" aria-hidden="true"></i>
    </span>
  </div>
  <div  class="input-group">
      <?php echo form_error('numero', '<div class="text-center p-t-12 txterror">', '</div>'); ?>
  </div>

  <div class="wrap-input80">
    <textarea class='textarea input100' rows='4' id='observaciones' name='observaciones' placeholder='Observaciones'></textarea>
    <span class="focus-input100"></span>
    <span class="symbol-input100">
      <i class="fa fa-edit" aria-hidden="true"></i>
    </span>
  </div>
  <div  class="input-group">
      <?php echo form_error('observaciones', '<div class="text-center p-t-12 txterror">', '</div>'); ?>
  </div> 

  <div class="container-login50-form-btn">
    <input type="button" id="btnAceptar" class="login50-form-btn" value="ACEPTAR"/>
    <button type="button" class="registrar50-form-btn" id="btnServicios" onclick="window.open('<?php echo base_url()?>index.php/Usuarios/servicios_base')">VER SERVICIOS</button>
  </div>   

  <div class="text-center p-t-10">
    <span>Contacto</span>
  </div>
  <div class="text-center">
    <span>Vargas Group 3121069034</span>
  </div>

</div>


  <script>

    $( document ).ready(function() {
      estado = "<?php echo $this->session->userdata['loggedin']['estado'];?>"; //$("#hdEstado").val();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url() ?>index.php/Usuarios/obtieneCatEstados",
        dataType: "json",
        success: function (data) {
          strCmbEstado = "<option value='0' disabled selected hidden>ESTADO</option>";
        
          for (var i = 0, len = data['estados'].length; i < len; i++) 
          {
            if(data['estados'][i]['c_Estado'] == estado)
            {
              strCmbEstado = strCmbEstado + "<option value='"+data['estados'][i]['c_Estado']+"' selected>"+data['estados'][i]['Nombre'].toUpperCase()+"</option>";
            }
            else
            {
              strCmbEstado = strCmbEstado + "<option value='"+data['estados'][i]['c_Estado'].toUpperCase()+"'>"+data['estados'][i]['Nombre']+"</option>";
            }
          }

          $("#cmbEstado").html(strCmbEstado);
        },
        error: function(xhr, ajaxOptions, throwError){
          alert(throwError);
        }
      });
      obtieneMunicipio();
      obtieneLocalidad();
      $("#myModal").modal({backdrop: "static"});
    });

    function obtieneMunicipio()
    {
      entidad = "<?php echo $this->session->userdata['loggedin']['estado'];?>";//$("#cmbEstado").val();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url() ?>index.php/Usuarios/obtieneCatMunicipios/"+entidad,
        dataType: "json",
        success: function (data) {
          strCmbMunicipio = "<option value='0' disabled selected hidden>MUNICIPIO</option>";
        
          for (var i = 0, len = data['municipios'].length; i < len; i++) 
          {
            strCmbMunicipio = strCmbMunicipio + "<option value='"+data['municipios'][i]['c_Municipio']+"'>"+data['municipios'][i]['Nombre'].toUpperCase()+"</option>";
          }
          $("#cmbMunicipio").html(strCmbMunicipio);
        },
        error: function(xhr, ajaxOptions, throwError){
          alert(throwError);
        }
      });
    }

    function obtieneLocalidad()
    {
      entidad = "<?php echo $this->session->userdata['loggedin']['estado'];?>";//$("#cmbEstado").val();
      municipio = $("#cmbMunicipio").val();
      
      $.ajax({
        type: "POST",
        url: "<?php echo base_url() ?>index.php/Usuarios/obtieneCatLocalidades/"+entidad+"/"+municipio,
        dataType: "json",
        success: function (data) {
          strCmbLocalidad = "<option value='0' disabled selected hidden>LOCALIDAD</option>";
        
          for (var i = 0, len = data['localidades'].length; i < len; i++) 
            {
            strCmbLocalidad = strCmbLocalidad + "<option value='"+data['localidades'][i]['CVE_LOC']+"'>"+data['localidades'][i]['NOM_LOC'].toUpperCase()+"</option>";

            
          }
          $("#cmbLocalidad").html(strCmbLocalidad);
        },
        error: function(xhr, ajaxOptions, throwError){
          alert(throwError);
        }
      });
    }

    function obtieneColonia()
    {
      entidad = $("#cmbEstado").val();
      municipio = $("#cmbMunicipio").val();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url() ?>index.php/Usuarios/obtieneCatColonias/"+entidad+"/"+municipio,
        dataType: "json",
        success: function (data) {
          strCmbColonia = "<option value='0' disabled selected hidden>COLONIA</option>";
        
          for (var i = 0, len = data['colonias'].length; i < len; i++) 
            {
            strCmbColonia = strCmbColonia + "<option value='"+data['colonias'][i]['c_Colonia']+"'>"+data['colonias'][i]['Nombre'].toUpperCase()+"</option>";

            
          }
          $("#cmbColonia").html(strCmbColonia);
        },
        error: function(xhr, ajaxOptions, throwError){
          alert(throwError);
        }
      });
    }
    

    var markers = [];
    var map;
    var markeruser;
    var infowindow;

    function initMap() {
      nlat = parseFloat("19.25566608394677"); 
      nlng = parseFloat("-103.7515141069153");

      // Create a map object and specify the DOM element for display.
      map = new google.maps.Map(document.getElementById('map-canvas'), {
          center: {lat: nlat, lng: nlng},
          zoom: 15, 
          gestureHandling: 'greedy', 
          disableDefaultUI: true
        });

         google.maps.event.trigger(map, 'resize');
      }

  function initMapWithMarker(nlat, nlng) {
      
      // Create a map object and specify the DOM element for display.
      map = new google.maps.Map(document.getElementById('map-canvas'), {
          center: {lat: nlat, lng: nlng},
          zoom: 15, 
          gestureHandling: 'greedy', 
          disableDefaultUI: true
        });

        var iconBase = '<?php echo base_url() ?>assets/images/logos/';
        var icons = {
          library: {
            icon: iconBase + 'user.png'
          }
        };

        var userLatLng = new google.maps.LatLng(nlat, nlng);

        var features = [
        {
          position: userLatLng,
          type: 'library'
        } ];

        writeAddressName(userLatLng);

          // Create markers.
         features.forEach(function(feature) {
            
           markeruser = new google.maps.Marker({
              position: feature.position,
              icon: icons[feature.type].icon,
              map: map, 
              draggable:true 
            });
            
            google.maps.event.addListener(markeruser, "dragstart", closeMapInfoWindow );
            google.maps.event.addListener(markeruser, 'dragend', function (event){
                $("#hdLatitud").val(this.getPosition().lat());
                $("#hdLongitud").val(this.getPosition().lng());
                writeAddressName(new google.maps.LatLng(this.getPosition().lat(), this.getPosition().lng()));

              })
          });

        var centerControlDiv = document.createElement('div');
        var centerControl = new CenterControl(centerControlDiv, map);

        centerControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);
        
         google.maps.event.trigger(map, 'resize');
      }

      function CenterControl(controlDiv, map) {

        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#57cbcc';
        controlUI.style.border = '2px solid #57cbcc';
        controlUI.style.borderRadius = '20px';
        controlUI.style.boxShadow = '0 6px 6px rgba(0,0,0,.3)';
        controlUI.style.cursor = 'pointer';
        controlUI.style.marginBottom = '22px';
        controlUI.style.marginTop = '15px';
        controlUI.style.textAlign = 'center';
        controlUI.title = 'Click para continuar con la solicitud'; 
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.color = 'rgb(255, 255, 255)';
        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '18px';
        controlText.style.padding = '14px';
        controlText.innerHTML = 'SOLICITAR';
        controlUI.appendChild(controlText);

        // Setup the click event listeners: simply set the map to Chicago.
        controlUI.addEventListener('click', function() {
          solicitarform.submit();
        });

    }

      function closeMapInfoWindow() {infowindow.close(); }

    function writeAddressName(latLng) {

      $.ajax({
        type : 'POST',
        data: '', 
        url: "https://maps.googleapis.com/maps/api/geocode/json?latlng="+parseFloat($("#hdLatitud").val())+","+parseFloat($("#hdLongitud").val())+"&key=AIzaSyBnlbm19GWCdNHou3G7wSo4890x5wm7XoE", 
        success: function(result){
            

            var address = result['results'][0]['formatted_address'];
            $("#hdDomicilio").val(address);      
            console.log(address);
            //document.getElementById("address").innerHTML = address;
            var contentString = '<div id="content" class="contentMap">'+address+'</div>';

            infowindow = new google.maps.InfoWindow({
              content: contentString
            });
            infowindow.open(map, markeruser);
        }});
    }

    function valida(e){
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla ==8 || tecla==9){
            return true;
        }
            
        // Patron de entrada, en este caso solo acepta numeros
        patron =/[0-9]/;
        //alert(tecla);
        tecla_final = String.fromCharCode(tecla);
        //alert(tecla_final);
        return patron.test(tecla_final);
        
        /*var key = evt.keyCode;
        alert(key);
        return (key >= 48 && key <= 57);*/

    }

    $('#btnAceptar').click(function(e) {
      nombre = $("#nombre").val();
      telefono = $("#telefono").val();
      observaciones = $("#observaciones").val();

      calle = $("#calle").val();
      numero = $("#numero").val();
      
      var combo;

      combo = document.getElementById("cmbColonia");
      colonia = combo.options[combo.selectedIndex].label;
      cve_colonia = combo.options[combo.selectedIndex].value;

      combo = document.getElementById("cmbLocalidad");
      localidad = combo.options[combo.selectedIndex].label;
      cve_localidad = combo.options[combo.selectedIndex].value;

      combo = document.getElementById("cmbMunicipio");
      municipio = combo.options[combo.selectedIndex].label;

      combo = document.getElementById("cmbEstado");
      estado = combo.options[combo.selectedIndex].label;
      
      domicilio = numero+' '+calle+', '+colonia+', '+localidad+', '+municipio+', '+estado;
      domicilio = domicilio.replace(/\s/g, "+");

      if(nombre == '') 
      {
        alert("Debe capturar el nombre");
        $("#nombre").focus();
        return;
      }
      if(telefono == '')
      {
        alert("Debe capturar el telefono");
        $("#telefono").focus();
        return;
      } 
      if(cve_localidad == '0')
      {
        alert("Debe seleccionar la localidad");
        $("#cmbLocalidad").focus();
        return;
      } 
      if(cve_colonia == '0')
      {
        alert("Debe seleccionar la colonia");
        $("#cmbColonia").focus();
        return;
      }
      if(calle == '')
      {
        alert("Debe capturar la calle");
        $("#calle").focus();
        return;
      } 

      $("#hdTelefono").val(telefono);
      $("#hdNombre").val(nombre);
      $("#hdObservaciones").val(observaciones);
      domicilio2 = calle+' '+numero+', '+colonia+', '+localidad+', '+municipio+', '+estado;

      $("#hdDomicilioUsuario").val(domicilio2.toUpperCase());
     
      $.ajax({
        type : 'POST',
        data: '', 
        url: "https://maps.googleapis.com/maps/api/geocode/json?address="+domicilio+"&key=AIzaSyBnlbm19GWCdNHou3G7wSo4890x5wm7XoE", 
        success: function(result){
            i = result['results'].length-1;
            latitud = result['results'][i]['geometry']['location']['lat'];
            longitud = result['results'][i]['geometry']['location']['lng'];
            $("#hdLatitud").val(latitud);
            $("#hdLongitud").val(longitud);

            $("#myModal").modal('hide');
            initMapWithMarker(latitud, longitud); 
                     
        }});
      });


    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnlbm19GWCdNHou3G7wSo4890x5wm7XoE&callback=initMap">
  </script>
  </body>

</html>