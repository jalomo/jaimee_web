
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Operadores_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function existe_operador($data) {
		$condition = "operadorUsuario = '" . $data['usuario'] . "' and operadorStatus = 1";
		$this->db->select('*');
		$this->db->from('operador');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}

	function insert_operador($post_data) {
		
    	$this->db->insert('operador',$post_data);
    	return $this->db->insert_id();
	}

	public function existe_auto($placa) {
		$condition = "autosPlacas = '" . $placa . "' and autosStatus = 1";
		$this->db->select('*');
		$this->db->from('autos');
		$this->db->where($condition);
		$query = $this->db->get();
		/*print $query->num_rows();
		print_r($query->result());*/
		if ($query->num_rows() != 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	function insert_auto($post_data) {
		
    	$this->db->insert('autos',$post_data);
    	return $this->db->insert_id();
	}

	function insert_operador_auto($post_data) {
		
    	$this->db->insert('operador_autos',$post_data);
    	return $this->db->insert_id();
	}

	public function buscarparcial($texto, $contar, $limit = null, $start = null)
	{
		
		$condition = "operadorNombreCompleto like '%" . $texto . "%' and operadorStatus = 1 ";

		$this->db->select('*');
	    $this->db->from('operador');
	    $this->db->where($condition);

	    if ($contar == 0)
	    	$this->db->limit($limit, $start);

		$this->db->order_by('operadorId');
	    $query = $this->db->get();

	    if ($contar == 1)
			return $query->num_rows();
		else
			if ($query->num_rows() > 0 ) {
				return $query->result();
			} else {
				return false;
			}
	}

	public function buscarparcialveh($texto)
	{
		
		$condition = "autosPlacas like '" . $texto . "%' and autosStatus = 1 ";

		$this->db->select('*');
	    $this->db->from('autos');
	    $this->db->where($condition);

	    
		$this->db->order_by('autosId');
	    $query = $this->db->get();


			if ($query->num_rows() > 0 ) {
				return $query->result();
			} else {
				return false;
			}
	}

	public function datosOperadorAuto($idOperador)
	{
		$this->db->select('operador.OperadorNombreCompleto, autos.autosPlacas, autos.autosDescripcion, autos.autosSitio, autos.autosNick');
	    $this->db->from('operador');
	    $this->db->join('operador_autos', 'operador_autos.OPid = operadorId'); 
	    $this->db->join('autos', 'autos.autosId = operador_autos.OPIdAuto'); 
	    $this->db->where("operadorId = '".$idOperador."'");
	    $query1 = $this->db->get();

	    if ($query1->num_rows() != 0) {
			return $query1->result();
		} else {
			return false;
		}
	}

	function get_operadores()
	{
		$this->db->select('operador_autos.OPLatitud, operador_autos.OPLongitud, operador_autos.OPIdOperador, operador_autos.OPIdAuto, operador.OperadorNombreCompleto, operador.OperadorImagen, operador.operadorTelefono, autos.autosPlacas, autos.autosDescripcion, autos.autosImagen, autos.autosSitio, autos.autosNick, autos.autosColor, operador.OperadorImagen2, autos.autosTipo, operador_autos.OPStatust');
	    $this->db->from('operador_autos');
	    $this->db->join('operador', 'operador.operadorId = operador_autos.OPIdOperador'); 
	    $this->db->join('autos', 'autos.autosId = operador_autos.OPIdAuto'); 

	    $where = "operador_autos.OPLatitud != '' and operadorStatus = 1 and operador_autos.OPStatust in (0,1) and DATE_FORMAT(fecha_actualizacion, '%d/%m/%Y') = DATE_FORMAT(now(), '%d/%m/%Y')";
	    $this->db->where($where);

	    $query1 = $this->db->get();

	    return $query1->result();
	}
}