<?php
	
	class Notificaciones {

		public function notificar($data_notifica)
	    {
	        //$this->output->unset_template();

	        try {
	            $curl = curl_init();
	            curl_setopt_array($curl, array(
	                CURLOPT_RETURNTRANSFER => 1,
	                CURLOPT_POST => 1, 
	                CURLOPT_URL => 'http://ordendeservicio.com/mensajes/index.php/api/enviar_notificacion',
	                CURLOPT_USERAGENT => 'cURL Request', 
	                CURLOPT_POSTFIELDS => 'numero='.$data_notifica['telefonoCliente'].'&empresa='.$data_notifica['idEmpresa'].'&email='.$data_notifica['emailCliente'].'&mensaje='.$data_notifica['mensaje'] 
	            ));
	            $resp = curl_exec($curl);
	            curl_close($curl);
	            //print_r($resp);

	            $data_codigo['return'] = 1;
	            $data_codigo['codigo'] = "El cliente ha sido notificado";
	            return $data_codigo;

	        }catch(Exception $ex){
	            $data_codigo['return'] = 0;
	            $data_codigo['codigo'] = $ex->getMessage();
	            //error_log(date("d-m-Y h:i:s"). " - ".$ex->getMessage()."\n", 3, self::USER_ERROR_DIR); 
	            return $data_codigo;
	        }
	        //print_r($data_codigo);
	    }
	}
?>